<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Page</h1>
                            <div class="heading__time">System time:  <strong>2019-07-16 14:14:48</strong></div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn btn_yellow btn_xs ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/board.inc.php') ?>

                    <div class="content">

                        <ul class="table_filter">
                            <li>
                                <a href="#" data-value="1" class="active">
                                    <i>
                                        <img src="img/table_filter__icon_01.png" alt="">
                                    </i>
                                    <span>Refill</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" data-value="2">
                                    <i>
                                        <img src="img/table_filter__icon_02.png" alt="">
                                    </i>
                                    <span>Payout</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" data-value="3">
                                    <i>
                                        <img src="img/table_filter__icon_03.png" alt="">
                                    </i>
                                    <span>Ref. income</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" data-value="4">
                                    <i>
                                        <img src="img/table_filter__icon_04.png" alt="">
                                    </i>
                                    <span>Money transfer</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" data-value="5">
                                    <i>
                                        <img src="img/table_filter__icon_05.png" alt="">
                                    </i>
                                    <span>Exchange</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" data-value="6">
                                    <i>
                                        <img src="img/table_filter__icon_06.png" alt="">
                                    </i>
                                    <span>Packages</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" data-value="7">
                                    <i>
                                        <img src="img/table_filter__icon_07.png" alt="">
                                    </i>
                                    <span>New level</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" data-value="8">
                                    <i>
                                        <img src="img/table_filter__icon_08.png" alt="">
                                    </i>
                                    <span>Partnership activation</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" data-value="9">
                                    <i>
                                        <img src="img/table_filter__icon_09.png" alt="">
                                    </i>
                                    <span>Bal. trans. inv.</span>
                                </a>
                            </li>
                        </ul>

                        <div class="content__table">
                            <div class="table_responsive">
                                <table class="table">
                                    <tr>
                                        <th>Date</th>
                                        <th>Type of operation</th>
                                        <th>Amount</th>
                                        <th class="text-nowrap text-right">Status</th>
                                    </tr>

                                    <tr>
                                        <td class="text-nowrap">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap">Refill</td>
                                        <td class="text-nowrap"><span class="color_green">+ $540.00</span></td>
                                        <td class="text-nowrap text-right"><span class="status_item status_item_wait">Waiting</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap">Refill</td>
                                        <td class="text-nowrap"><span class="color_green">+ $540.00</span></td>
                                        <td class="text-nowrap text-right"><span class="status_item status_item_done">Done</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap">Refill</td>
                                        <td class="text-nowrap"><span class="color_green">+ $540.00</span></td>
                                        <td class="text-nowrap text-right"><span class="status_item status_item_done">Done</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap">Refill</td>
                                        <td class="text-nowrap"><span class="color_red">- $640.00</span></td>
                                        <td class="text-nowrap text-right"><span class="status_item status_item_done">Done</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap">Payout</td>
                                        <td class="text-nowrap"><span class="color_green"></span></td>
                                        <td class="text-nowrap text-right"><span class="status_item status_item_reject">Rejected</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap">Refill</td>
                                        <td class="text-nowrap"><span class="color_green">+ $540.00</span></td>
                                        <td class="text-nowrap text-right"><span class="status_item status_item_wait">Waiting</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap">Refill</td>
                                        <td class="text-nowrap"><span class="color_green">+ $540.00</span></td>
                                        <td class="text-nowrap text-right"><span class="status_item status_item_done">Done</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap">Refill</td>
                                        <td class="text-nowrap"><span class="color_green">+ $540.00</span></td>
                                        <td class="text-nowrap text-right"><span class="status_item status_item_done">Done</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap">Refill</td>
                                        <td class="text-nowrap"><span class="color_red">- $640.00</span></td>
                                        <td class="text-nowrap text-right"><span class="status_item status_item_done">Done</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap">Payout</td>
                                        <td class="text-nowrap"><span class="color_green"></span></td>
                                        <td class="text-nowrap text-right"><span class="status_item status_item_reject">Rejected</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap">Refill</td>
                                        <td class="text-nowrap"><span class="color_green">+ $540.00</span></td>
                                        <td class="text-nowrap text-right"><span class="status_item status_item_wait">Waiting</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap">Refill</td>
                                        <td class="text-nowrap"><span class="color_green">+ $540.00</span></td>
                                        <td class="text-nowrap text-right"><span class="status_item status_item_done">Done</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap">Refill</td>
                                        <td class="text-nowrap"><span class="color_green">+ $540.00</span></td>
                                        <td class="text-nowrap text-right"><span class="status_item status_item_done">Done</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap">Refill</td>
                                        <td class="text-nowrap"><span class="color_red">- $640.00</span></td>
                                        <td class="text-nowrap text-right"><span class="status_item status_item_done">Done</span></td>
                                    </tr>
                                 </table>
                            </div>
                        </div>

                        <ul class="pagination">
                            <li><a href="#" class="pagination_prev"></a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><span class="pagination_space">...</span></li>
                            <li><a href="#">9</a></li>
                            <li><a href="#" class="pagination_next"></a></li>
                        </ul>

                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>


        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
