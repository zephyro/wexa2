<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Tickets</h1>
                            <div class="heading__time">System time:  <strong>2019-07-16 14:14:48</strong></div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn btn_yellow btn_xs ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <div class="content">

                        <div class="content__header">
                            <div class="content__header_title">
                                <h4>Tickets list: 34</h4>
                            </div>
                            <div class="content__header_data">
                                <a href="#" class="btn btn_sm btn_green btn_plus">Create ticket</a>
                            </div>
                        </div>

                        <div class="content__table mb_30">
                            <div class="table_responsive">
                                <table class="table">
                                    <tr>
                                        <th>Ticket ID</th>
                                        <th class="min_width_200">Subject</th>
                                        <th>Created</th>
                                        <th>Last reply</th>
                                        <th class="text-right">Status</th>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">NMD32562</td>
                                        <td><a href="#" class="table_link">Withdrawal payment Bitcoin error</a></td>
                                        <td class="text-nowrap">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap">24 June 2019, at 12:10</td>
                                        <td class="text-nowrap text-right"><span class="status status_new">new</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">MFK93044</td>
                                        <td><a href="#" class="table_link">Test account Balance</a></td>
                                        <td class="text-nowrap">12 May 2019, at 10:34</td>
                                        <td class="text-nowrap">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap text-right"><span class="status status_new">new</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">KKFS83911</td>
                                        <td><a href="#" class="table_link">New store addition</a></td>
                                        <td class="text-nowrap">30 Dec 2018, at 20:39</td>
                                        <td class="text-nowrap">12 May 2019, at 10:34</td>
                                        <td class="text-nowrap text-right"><span class="status status_new">new</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">NMD32562</td>
                                        <td><a href="#" class="table_link">Withdrawal Errors</a></td>
                                        <td class="text-nowrap">17 Nov 2018, at 10:34</td>
                                        <td class="text-nowrap">30 Dec 2018, at 20:39</td>
                                        <td class="text-nowrap text-right"><span class="status status_support">support reply</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">MFK93044</td>
                                        <td><a href="#" class="table_link">Bank Account Verification</a></td>
                                        <td class="text-nowrap">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap">17 Nov 2018, at 10:34</td>
                                        <td class="text-nowrap text-right"><span class="status status_support">support reply</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap color_gray">KKFS83911</td>
                                        <td class="color_gray">Withdrawal payment error</td>
                                        <td class="text-nowrap color_gray">12 May 2019, at 10:34</td>
                                        <td class="text-nowrap color_gray">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap text-right"><span class="status status_close">closed</span></td>
                                    </tr>


                                    <tr>
                                        <td class="text-nowrap">NMD32562</td>
                                        <td><a href="#" class="table_link">Withdrawal payment Bitcoin error</a></td>
                                        <td class="text-nowrap">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap">24 June 2019, at 12:10</td>
                                        <td class="text-nowrap text-right"><span class="status status_reply">user reply</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">MFK93044</td>
                                        <td><a href="#" class="table_link">Test account Balance</a></td>
                                        <td class="text-nowrap">12 May 2019, at 10:34</td>
                                        <td class="text-nowrap">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap text-right"><span class="status status_new">new</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">KKFS83911</td>
                                        <td><a href="#" class="table_link">New store addition</a></td>
                                        <td class="text-nowrap">30 Dec 2018, at 20:39</td>
                                        <td class="text-nowrap">12 May 2019, at 10:34</td>
                                        <td class="text-nowrap text-right"><span class="status status_new">new</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">NMD32562</td>
                                        <td><a href="#" class="table_link">Withdrawal Errors</a></td>
                                        <td class="text-nowrap">17 Nov 2018, at 10:34</td>
                                        <td class="text-nowrap">30 Dec 2018, at 20:39</td>
                                        <td class="text-nowrap text-right"><span class="status status_support">support reply</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">MFK93044</td>
                                        <td><a href="#" class="table_link">Bank Account Verification</a></td>
                                        <td class="text-nowrap">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap">17 Nov 2018, at 10:34</td>
                                        <td class="text-nowrap text-right"><span class="status status_support">support reply</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap color_gray">KKFS83911</td>
                                        <td class="color_gray">Withdrawal payment error</td>
                                        <td class="text-nowrap color_gray">12 May 2019, at 10:34</td>
                                        <td class="text-nowrap color_gray">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap text-right"><span class="status status_close">closed</span></td>
                                    </tr>

                                    <tr>
                                        <td class="text-nowrap">MFK93044</td>
                                        <td><a href="#" class="table_link">Test account Balance</a></td>
                                        <td class="text-nowrap">12 May 2019, at 10:34</td>
                                        <td class="text-nowrap">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap text-right"><span class="status status_new">new</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">KKFS83911</td>
                                        <td><a href="#" class="table_link">New store addition</a></td>
                                        <td class="text-nowrap">30 Dec 2018, at 20:39</td>
                                        <td class="text-nowrap">12 May 2019, at 10:34</td>
                                        <td class="text-nowrap text-right"><span class="status status_new">new</span></td>
                                    </tr>

                                </table>
                            </div>
                        </div>

                        <div class="text-center">
                            <a href="#" class="btn btn_sm btn_show_more">Show more</a>
                        </div>

                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>


        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
