<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Dashboard</h1>
                            <div class="heading__status">
                                <span class="user_status user_status__verified">Verified</span>
                                <span class="user_status user_status__processing">Processing</span>
                                <span class="user_status user_status__unverified">Unverified</span>
                            </div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn btn_yellow btn_xs ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <div class="dashboard">

                        <div class="row">

                            <div class="col col-xs-12 col-lg-6 col-gutter-lr">
                                <div class="row">
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="informer informer_blue">
                                            <div class="informer__large">
                                                <div class="informer__data">
                                                    <span>MAIN BALANCE</span>
                                                    <strong>$ 259.50</strong>
                                                </div>
                                                <div class="dashboard_icon">
                                                    <img src="img/dashboard__icon_01.png" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="informer informer_green">
                                            <div class="informer__large">
                                                <div class="informer__data">
                                                    <span>FUND BALANCE</span>
                                                    <strong>$ 10.00</strong>
                                                </div>
                                                <div class="dashboard_icon">
                                                    <img src="img/dashboard__icon_02.png" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="informer informer_yellow">
                                            <div class="informer__large">
                                                <div class="informer__data">
                                                    <span>AFFILIATE BALANCE</span>
                                                    <strong>$ 330.75</strong>
                                                </div>
                                                <div class="dashboard_icon">
                                                    <img src="img/dashboard__icon_03.png" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="informer informer_white">
                                            <div class="informer__mini">
                                                <div class="informer__mini_elem informer__mini_01">
                                                    <span>BASIC</span>
                                                    <strong>$ 120.00</strong>
                                                </div>
                                                <div class="informer__mini_elem informer__mini_02">
                                                    <span>EXCLUSIVE</span>
                                                    <strong>$ 0.75</strong>
                                                </div>
                                                <div class="informer__mini_elem informer__mini_03">
                                                    <span>PREMIUM</span>
                                                    <strong>$ 60.00</strong>
                                                </div>
                                                <div class="informer__mini_elem informer__mini_04">
                                                    <span>POINTS</span>
                                                    <strong>$ 1,250.75</strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col col-xs-12 col-lg-6 col-gutter-lr">
                                <div class="row">
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="informer informer_lg">

                                            <div class="informer__stage">
                                                <div class="informer__stage_data">
                                                    <strong>5</strong>
                                                    <span>level</span>
                                                </div>
                                                <ul class="informer__stage_text">
                                                    <li>Ref. percent: <strong>6.00%</strong></li>
                                                    <li>Next bonus: <strong>$ 200</strong></li>
                                                    <li>Need turnover: <strong>$ 10,000</strong></li>
                                                </ul>
                                            </div>

                                            <div class="informer__chart">

                                                <div class="informer__chart_item">
                                                    <div class="informer__chart_doughnut">
                                                        <canvas id="myChart1" width="80" height="80"></canvas>
                                                        <div class="informer__chart_value">
                                                            <div class="informer__chart_value_wrap">
                                                                <strong>1st</strong>
                                                                <span>LEG</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="informer__chart_legend">
                                                        <div><strong>167</strong> / <span>5000</span></div>
                                                        <div>Sales</div>
                                                    </div>
                                                </div>

                                                <div class="informer__chart_item">
                                                    <div class="informer__chart_doughnut">
                                                        <canvas id="myChart2" width="80" height="80"></canvas>
                                                        <div class="informer__chart_value">
                                                            <div class="informer__chart_value_wrap">
                                                                <strong>2st</strong>
                                                                <span>LEG</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="informer__chart_legend">
                                                        <div><strong>2765</strong> / <span>5000</span></div>
                                                        <div>Sales</div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="informer informer_lg">
                                            <div class="informer__notice">
                                                <div class="informer__notice_heading">Notifications</div>
                                                <div class="informer__notice_scroll">
                                                    <ul class="notice_line">
                                                        <li>
                                                            <div class="notice_line__title">System update 2.0</div>
                                                            <div class="notice_line__text">We are a team of technology and finance hotheads. Our passion are cryptocurrencies</div>
                                                            <div class="notice_line__date">22 June 2019</div>
                                                        </li>
                                                        <li>
                                                            <div class="notice_line__title">New Level 5</div>
                                                            <div class="notice_line__text">Our passion are cryptocurrencies and their role in changing world.</div>
                                                            <div class="notice_line__date">16 June 2019</div>
                                                        </li>
                                                        <li>
                                                            <div class="notice_line__title">System update 2.0</div>
                                                            <div class="notice_line__text">We are a team of technology and finance hotheads. Our passion are cryptocurrencies</div>
                                                            <div class="notice_line__date">22 June 2019</div>
                                                        </li>
                                                        <li>
                                                            <div class="notice_line__title">New Level 5</div>
                                                            <div class="notice_line__text">Our passion are cryptocurrencies and their role in changing world.</div>
                                                            <div class="notice_line__date">16 June 2019</div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="informer">
                            <ul class="informer__index">
                                <li>
                                    <div class="informer__index_image">
                                        <div class="informer__index_icon">
                                            <img src="img/index__icon_01.png" class="img-fluid" alt="">
                                        </div>
                                        <div class="informer__index_bg">
                                            <img src="img/index__icon_bg.png" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="informer__index_text">
                                        <strong>$ 310.00</strong>
                                        <span>My packages</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="informer__index_image">
                                        <div class="informer__index_icon">
                                            <img src="img/index__icon_02.png" class="img-fluid" alt="">
                                        </div>
                                        <div class="informer__index_bg">
                                            <img src="img/index__icon_bg.png" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="informer__index_text">
                                        <strong>$ 1,528.00</strong>
                                        <span>Affiliate income</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="informer__index_image">
                                        <div class="informer__index_icon">
                                            <img src="img/index__icon_03.png" class="img-fluid" alt="">
                                        </div>
                                        <div class="informer__index_bg">
                                            <img src="img/index__icon_bg.png" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="informer__index_text">
                                        <strong>$ 3,456.50</strong>
                                        <span>total Sales</span>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div class="row">
                            <div class="col col-xs-12 col-lg-6 col-gutter-lr">
                                <div class="informer">
                                    <div class="informer_chart">
                                        <div class="informer_chart__legend">
                                            <div class="informer_chart__legend_title">Partners</div>
                                            <ul class="informer_chart__legend_values">
                                                <li><strong>88</strong> <span>Total partners</span></li>
                                                <li><strong>36</strong> <span>Active partners</span></li>
                                            </ul>
                                        </div>
                                        <div class="informer_chart__content">
                                            <canvas id="myLine"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <div class="informer">
                                    <div class="informer_chart">
                                        <div class="informer_chart__legend">
                                            <div class="informer_chart__legend_title">Weekly turnover</div>
                                            <ul class="informer_chart__legend_values">
                                                <li class="no_point"><strong>$345.00</strong> <span>Total turnover</span></li>
                                            </ul>
                                        </div>
                                        <div class="informer_chart__content">
                                            <div class="chart">
                                                <ul class="bars" id="bars">
                                                    <li><div data-percentage="56" class="bar" style="height: 56%;"></div><span>28.04.19</span></li>
                                                    <li><div data-percentage="33" class="bar" style="height: 33%;"></div><span>29.04.19</span></li>
                                                    <li><div data-percentage="54" class="bar" style="height: 54%;"></div><span>30.04.19</span></li>
                                                    <li><div data-percentage="91" class="bar" style="height: 91%;"></div><span>1.05.19</span></li>
                                                    <li><div data-percentage="44" class="bar" style="height: 44%;"></div><span>2.05.19</span></li>
                                                    <li><div data-percentage="23" class="bar" style="height: 23%;"></div><span>3.05.19</span></li>
                                                    <li><div data-percentage="55" class="bar" style="height: 55%;"></div><span>4.05.19</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-lg-6 col-gutter-lr">
                                <div class="box">
                                    <div class="box__heading">Latest Registrations</div>
                                    <div class="box__content">
                                        <div class="table_box">
                                            <div class="table_box__header">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <span>Partner ID</span>
                                                        </td>
                                                        <td>
                                                            <span>Upline</span>
                                                        </td>
                                                        <td>
                                                            <span>Inviter</span>
                                                        </td>
                                                        <td>
                                                            <span>Date</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="table_box__scroll">
                                                <div class="table_box__content">
                                                    <table>
                                                        <tr>
                                                            <td data-header="Partner ID">
                                                                <div class="table_box__middle">
                                                                    <i>
                                                                        <img src="img/icon_table__user.png" class="img-fluid">
                                                                    </i>
                                                                    <span>Nazar</span>
                                                                </div>
                                                            </td>
                                                            <td data-header="Upline"><span>VX271962</span></td>
                                                            <td data-header="Inviter"><span>VX271962</span></td>
                                                            <td data-header="Date"><span class="color_gray">22 june 2019</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td data-header="Partner ID">
                                                                <div class="table_box__middle">
                                                                    <i>
                                                                        <img src="img/icon_table__user.png" class="img-fluid">
                                                                    </i>
                                                                    <span>olgamuzikant</span>
                                                                </div>
                                                            </td>
                                                            <td data-header="Upline"><span>VX271962</span></td>
                                                            <td data-header="Inviter"><span>VX271962</span></td>
                                                            <td data-header="Date"><span class="color_gray">22 june 2019</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td data-header="Partner ID">
                                                                <div class="table_box__middle">
                                                                    <i>
                                                                        <img src="img/icon_table__user.png" class="img-fluid">
                                                                    </i>
                                                                    <span>ladyrich88</span>
                                                                </div>
                                                            </td>
                                                            <td data-header="Upline"><span>VX271962</span></td>
                                                            <td data-header="Inviter"><span>VX271962</span></td>
                                                            <td data-header="Date"><span class="color_gray">22 june 2019</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td data-header="Partner ID">
                                                                <div class="table_box__middle">
                                                                    <i>
                                                                        <img src="img/icon_table__user.png" class="img-fluid">
                                                                    </i>
                                                                    <span>Mady</span>
                                                                </div>
                                                            </td>
                                                            <td data-header="Upline"><span>VX271962</span></td>
                                                            <td data-header="Inviter"><span>VX271962</span></td>
                                                            <td data-header="Date"><span class="color_gray">22 june 2019</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td data-header="Partner ID">
                                                                <div class="table_box__middle">
                                                                    <i>
                                                                        <img src="img/icon_table__user.png" class="img-fluid">
                                                                    </i>
                                                                    <span>jeine25</span>
                                                                </div>
                                                            </td>
                                                            <td data-header="Upline"><span>VX271962</span></td>
                                                            <td data-header="Inviter"><span>VX271962</span></td>
                                                            <td data-header="Date"><span class="color_gray">22 june 2019</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td data-header="Partner ID">
                                                                <div class="table_box__middle">
                                                                    <i>
                                                                        <img src="img/icon_table__user.png" class="img-fluid">
                                                                    </i>
                                                                    <span>seba</span>
                                                                </div>
                                                            </td>
                                                            <td data-header="Upline"><span>VX271962</span></td>
                                                            <td data-header="Inviter"><span>VX271962</span></td>
                                                            <td data-header="Date"><span class="color_gray">22 june 2019</span></td>
                                                        </tr>

                                                        <tr>
                                                            <td data-header="Partner ID">
                                                                <div class="table_box__middle">
                                                                    <i>
                                                                        <img src="img/icon_table__user.png" class="img-fluid">
                                                                    </i>
                                                                    <span>Nazar</span>
                                                                </div>
                                                            </td>
                                                            <td data-header="Upline"><span>VX271962</span></td>
                                                            <td data-header="Inviter"><span>VX271962</span></td>
                                                            <td data-header="Date"><span class="color_gray">22 june 2019</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td data-header="Partner ID">
                                                                <div class="table_box__middle">
                                                                    <i>
                                                                        <img src="img/icon_table__user.png" class="img-fluid">
                                                                    </i>
                                                                    <span>olgamuzikant</span>
                                                                </div>
                                                            </td>
                                                            <td data-header="Upline"><span>VX271962</span></td>
                                                            <td data-header="Inviter"><span>VX271962</span></td>
                                                            <td data-header="Date"><span class="color_gray">22 june 2019</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td data-header="Partner ID">
                                                                <div class="table_box__middle">
                                                                    <i>
                                                                        <img src="img/icon_table__user.png" class="img-fluid">
                                                                    </i>
                                                                    <span>ladyrich88</span>
                                                                </div>
                                                            </td>
                                                            <td data-header="Upline"><span>VX271962</span></td>
                                                            <td data-header="Inviter"><span>VX271962</span></td>
                                                            <td data-header="Date"><span class="color_gray">22 june 2019</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td data-header="Partner ID">
                                                                <div class="table_box__middle">
                                                                    <i>
                                                                        <img src="img/icon_table__user.png" class="img-fluid">
                                                                    </i>
                                                                    <span>Mady</span>
                                                                </div>
                                                            </td>
                                                            <td data-header="Upline"><span>VX271962</span></td>
                                                            <td data-header="Inviter"><span>VX271962</span></td>
                                                            <td data-header="Date"><span class="color_gray">22 june 2019</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td data-header="Partner ID">
                                                                <div class="table_box__middle">
                                                                    <i>
                                                                        <img src="img/icon_table__user.png" class="img-fluid">
                                                                    </i>
                                                                    <span>jeine25</span>
                                                                </div>
                                                            </td>
                                                            <td data-header="Upline"><span>VX271962</span></td>
                                                            <td data-header="Inviter"><span>VX271962</span></td>
                                                            <td data-header="Date"><span class="color_gray">22 june 2019</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td data-header="Partner ID">
                                                                <div class="table_box__middle">
                                                                    <i>
                                                                        <img src="img/icon_table__user.png" class="img-fluid">
                                                                    </i>
                                                                    <span>seba</span>
                                                                </div>
                                                            </td>
                                                            <td data-header="Upline"><span>VX271962</span></td>
                                                            <td data-header="Inviter"><span>VX271962</span></td>
                                                            <td data-header="Date"><span class="color_gray">22 june 2019</span></td>
                                                        </tr>

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="box__heading">Latest Referal Yield</div>
                                    <div class="box__content">
                                        <div class="table_box">
                                            <div class="table_box__header">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <span>Ref. Income</span>
                                                        </td>
                                                        <td>
                                                            <span>Invest</span>
                                                        </td>
                                                        <td>
                                                            <span>Partner</span>
                                                        </td>
                                                        <td>
                                                            <span>Date</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="table_box__scroll">
                                                <div class="table_box__content">
                                                    <table>
                                                        <tr>
                                                            <td data-header="Ref. Income"><span class="color_green text_lead">$14.27</span></td>
                                                            <td data-header="Invest"><span>$142.70</span></td>
                                                            <td data-header="Partner"><span>VX271962</span></td>
                                                            <td data-header="Date"><span class="color_gray">22 june 2019</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td data-header="Ref. Income"><span class="color_green text_lead">$4.14</span></td>
                                                            <td data-header="Invest"><span>$41.40</span></td>
                                                            <td data-header="Partner"><span>VX271962</span></td>
                                                            <td data-header="Date"><span class="color_gray">22 june 2019</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td data-header="Ref. Income"><span class="color_green text_lead">$7.05</span></td>
                                                            <td data-header="Invest"><span>$70.50</span></td>
                                                            <td data-header="Partner"><span>VX271962</span></td>
                                                            <td data-header="Date"><span class="color_gray">22 june 2019</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td data-header="Ref. Income"><span class="color_green text_lead">$5.30</span></td>
                                                            <td data-header="Invest"><span>$53.00</span></td>
                                                            <td data-header="Partner"><span>VX271962</span></td>
                                                            <td data-header="Date"><span class="color_gray">22 june 2019</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td data-header="Ref. Income"><span class="color_green text_lead">$5.00</span></td>
                                                            <td data-header="Invest"><span>$50.00</span></td>
                                                            <td data-header="Partner"><span>VX271962</span></td>
                                                            <td data-header="Date"><span class="color_gray">22 june 2019</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td data-header="Ref. Income"><span class="color_green text_lead">$117.75</span></td>
                                                            <td data-header="Invest"><span>$1177.50</span></td>
                                                            <td data-header="Partner"><span>VX271962</span></td>
                                                            <td data-header="Date"><span class="color_gray">22 june 2019</span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col col-xs-12 col-lg-6 col-gutter-lr">
                                <div class="box box__empty">
                                    <div class="box__heading">Latest Registrations</div>
                                    <div class="box__content">
                                        <div class="box__content_empty">
                                            <div class="box__empty_wrap">Nothing to display</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-lg-6 col-gutter-lr">
                                <div class="box box__empty">
                                    <div class="box__heading">Latest Referal Yield</div>
                                    <div class="box__content">
                                        <div class="box__content_empty">
                                            <div class="box__empty_wrap">Nothing to display</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>


        <?php include('inc/scripts.inc.php') ?>

        <script>

            // Chart 1
            var ctx1 = document.getElementById('myChart1').getContext('2d');
            var myChart1 = new Chart(ctx1, {
                type: 'doughnut',
                data: {
                    labels: ['Red', 'Blue'],
                    datasets: [{
                        label: '# of Votes',
                        data: [167, 4833],
                        backgroundColor: [
                            'rgba(0, 145, 255, 1)',
                            'rgba(227, 231, 239, 1)'
                        ],
                        borderColor: [
                            'rgba(0, 145, 255, 1)',
                            'rgba(227, 231, 239, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: true,
                    cutoutPercentage: 95,
                    legend: {
                        display: false
                    },
                    scales: {
                        display: false
                    }
                }
            });

            // Chart 2
            var ctx2 = document.getElementById('myChart2').getContext('2d');
            var myChart2 = new Chart(ctx2, {
                type: 'doughnut',
                data: {
                    labels: ['Red', 'Blue'],
                    datasets: [{
                        label: '# of Votes',
                        data: [2765, 2235],
                        backgroundColor: [
                            'rgba(0, 145, 255, 1)',
                            'rgba(34, 60, 94, 0.2)'
                        ],
                        borderColor: [
                            'rgba(0, 145, 255, 1)',
                            'rgba(227, 231, 239, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: true,
                    cutoutPercentage: 95,
                    legend: {
                        display: false
                    },
                    scales: {
                        display: false
                    }
                }
            });


            // Chart 3
            var ctx3 = document.getElementById('myLine').getContext('2d');

            var gradientLine = ctx3.createLinearGradient(0, 0, 0, 300);
            gradientLine.addColorStop(0, 'rgba(0,143,254,1)');
            gradientLine.addColorStop(1, 'rgba(255,255,255,0)');

            var myLineChart = new Chart(ctx3, {
                type: 'line',
                data: {
                    labels: ['28.04.19', '29.04.19', '30.04.19', '1.05.19', '2.05.19', '3.05.19', '4.05.19'],
                    datasets: [{
                        label: '',
                        data: [15, 30, 25, 20, 14, 28, 38],
                        borderColor: [
                            'rgba(24, 145, 255, 1)'
                        ],
                        backgroundColor: gradientLine,
                    }]
                },

                options: {
                    responsive: true,
                    legend: {
                        display: false
                    },
                    title: {
                        display: false,
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                        displayColors: false,
                        titleFontSize: 0,
                        titleSpacing: 0,
                        titleMarginBottom: 0,
                        bodySpacing: 12
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true
                        }],
                        yAxes: [{
                            display: true
                        }]
                    }
                }
            });


        </script>

    </body>
</html>
