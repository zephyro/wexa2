<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Tickets</h1>
                            <div class="heading__time">System time:  <strong>2019-07-16 14:14:48</strong></div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn btn_yellow btn_xs ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/board.inc.php') ?>

                    <div class="ticket">
                        <div class="ticket__header">
                            <div class="ticket__header_left">
                                <h2>Withdrawal Error</h2>
                                <div class="ticket__header_time">System time:  <strong>2019-07-16, at 14:14</strong></div>
                            </div>
                            <div class="ticket__header_right">
                                <div class="ticket__header_status">
                                    <span>Status:</span>
                                    <div class="ticket_status ticket_status__good">support reply</div>
                                </div>
                                <a href="#" class="btn">Close</a>
                            </div>
                        </div>
                        <div class="ticket__block">
                            <ul class="ticket__meta">
                                <li>
                                    <strong>ID</strong>
                                    <span>MNF293812</span>
                                </li>
                                <li>
                                    <strong>Category</strong>
                                    <span>Payments</span>
                                </li>
                            </ul>
                        </div>
                        <div class="ticket__block">
                            <h5>Description</h5>
                            <div class="ticket__text"><i>Hello! I want to withdraw my merchant Balance using Bitcoin BTC Wallet, but I have some error "Unable to process your phone number at this moment". I try to withdraw some later but I have same error. I want to withdraw my merchant Balance using Bitcoin BTC Wallet, but I have some error "Unable to process your phone number at this moment". I try to withdraw some later but I have same error. I want to withdraw my merchant Balance using Bitcoin BTC Wallet, but I have some error "Unable to process your phone number at this moment". I try to withdraw some later but I have same error. </i></div>
                        </div>
                        <div class="ticket__block">
                            <h5>Last reply</h5>
                            <p>2019-07-16, at 14:14</p>
                            <h5>Attachment</h5>
                            <ul>
                                <li><a href="#" class="attach_link">bitcoin_withdraw.png</a></li>
                                <li><a href="#" class="attach_link">bitcoin_withdra2.png</a></li>
                            </ul>
                        </div>
                        <div class="ticket__block">
                            <div class="ticket_post">
                                <div class="ticket_post__author">
                                    <div class="ticket_post__avatar">
                                        <img src="images/avatar.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="ticket_post__name"><a href="#">VexaSupport</a></div>
                                    <div class="ticket_post__date">(2019-07-16, at 14:14)</div>
                                </div>
                                <div class="ticket_post__content ticket_post__content_blue">
                                    <p>Hello,</p>
                                    <p>Thank you for contacting Vexa Global Support and for provided screenshot. I'm sorry to hear about the issue that you are having.</p>
                                    <p>Can you provide us with phone number which you use for payouts? Additionally please try again to withdraw funds and let us know date + time of last attempt for withdraw.</p>
                                    <p>I look forward to your reply.</p>
                                    <p>All the best,<br/>
                                        Martin<br/>
                                        Vexa Global Business Support
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="ticket__block">
                            <div class="ticket_post">
                                <div class="ticket_post__author">
                                    <div class="ticket_post__avatar">
                                        <img src="images/avatar.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="ticket_post__name">Myname</div>
                                    <div class="ticket_post__date">(2019-07-16, at 14:14)</div>
                                </div>
                                <div class="ticket_post__content">
                                    <p>Thanks for helping! Now it’s all works good!</p>
                                </div>
                            </div>
                        </div>
                        <div class="ticket__block">
                            <h5>New reply</h5>
                            <form class="form">
                                <div class="form_group">
                                    <label class="form_label">Message</label>
                                    <textarea class="form_control" name="message" placeholder="" rows="4"></textarea>
                                </div>
                                <div class="form_group mb_30">
                                    <div class="file_group">
                                        <label class="file_form">
                                            <input type="file" name="file">
                                            <span class="btn btn_green btn_file">Browse file</span>
                                        </label>
                                        <div class="file_text">
                                            (max 1 file, size 10 MB)<br>
                                            jpg, gif, png
                                        </div>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="button" class="btn btn_yellow btn_save">add reply</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>


        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
