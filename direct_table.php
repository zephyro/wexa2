<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Page</h1>
                            <div class="heading__time">System time:  <strong>2019-07-16 14:14:48</strong></div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn btn_yellow btn_xs ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/board.inc.php') ?>

                    <div class="content">

                        <div class="content__header">
                            <div class="content__header_title">
                                <h4>Direct partners</h4>
                            </div>
                            <div class="content__header_data">
                                <div class="search">
                                    <input class="form_control" type="text" name="search" value="">
                                    <button type="button" class="btn btn_yellow btn_xs btn_search">search</button>
                                </div>
                            </div>
                        </div>

                        <div class="table_responsive mb_30">
                            <table class="tree_table">

                                <tr>
                                    <th>Name</th>
                                    <th class="text-center">Line</th>
                                    <th class="text-center">Partners</th>
                                    <th class="text-center">Level</th>
                                    <th>Packages</th>
                                    <th>Sales</th>
                                    <th class="text-right">Refferal link</th>
                                </tr>

                                <tr data-level="0">
                                    <td>usa001</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$1,930.00</strong></td>
                                    <td class="text-nowrap"><strong></strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>

                                <tr data-level="1">
                                    <td>vg59365</td>
                                    <td class="text-center">2</td>
                                    <td class="text-center">37</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>vexa342</td>
                                    <td class="text-center">3</td>
                                    <td class="text-center">35</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>terry923</td>
                                    <td class="text-center">3</td>
                                    <td class="text-center">0</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>

                                <tr data-level="1">
                                    <td>vg59365</td>
                                    <td class="text-center">2</td>
                                    <td class="text-center">37</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>vexa342</td>
                                    <td class="text-center">3</td>
                                    <td class="text-center">35</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>terry923</td>
                                    <td class="text-center">3</td>
                                    <td class="text-center">0</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>


                                <tr data-level="0">
                                    <td>2 May 2019, at 10:34</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$1,930.00</strong></td>
                                    <td class="text-nowrap"><strong></strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="1">
                                    <td>vg59365</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="1">
                                    <td>vg59365</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="1">
                                    <td>vg59365</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>


                                <tr data-level="0">
                                    <td>30 Dec 2018, at 20:39</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$1,930.00</strong></td>
                                    <td class="text-nowrap"><strong></strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="1">
                                    <td>vg59365</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>vexa342</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>terry923</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="1">
                                    <td>vg59365</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>vexa342</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>terry923</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="1">
                                    <td>vg59365</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>vexa342</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>terry923</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>vexa342</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>terry923</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>

                                <tr data-level="0">
                                    <td>17 Nov 2018, at 10:34</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$1,930.00</strong></td>
                                    <td class="text-nowrap"><strong></strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="1">
                                    <td>vg59365</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>vexa342</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>terry923</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="1">
                                    <td>vg59365</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>vexa342</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>terry923</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>


                                <tr data-level="0">
                                    <td>usa001</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">46</td>
                                    <td class="text-center">2</td>
                                    <td class="text-nowrap"><strong>$1,930.00</strong></td>
                                    <td class="text-nowrap"><strong></strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>

                                <tr data-level="1">
                                    <td>vg59365</td>
                                    <td class="text-center">2</td>
                                    <td class="text-center">37</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>vexa342</td>
                                    <td class="text-center">3</td>
                                    <td class="text-center">35</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>terry923</td>
                                    <td class="text-center">3</td>
                                    <td class="text-center">0</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>vexa342</td>
                                    <td class="text-center">3</td>
                                    <td class="text-center">35</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>terry923</td>
                                    <td class="text-center">3</td>
                                    <td class="text-center">0</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>vexa342</td>
                                    <td class="text-center">3</td>
                                    <td class="text-center">35</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>terry923</td>
                                    <td class="text-center">3</td>
                                    <td class="text-center">0</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>


                                <tr data-level="1">
                                    <td>vg59365</td>
                                    <td class="text-center">2</td>
                                    <td class="text-center">37</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>vexa342</td>
                                    <td class="text-center">3</td>
                                    <td class="text-center">35</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>terry923</td>
                                    <td class="text-center">3</td>
                                    <td class="text-center">0</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>vexa342</td>
                                    <td class="text-center">3</td>
                                    <td class="text-center">35</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>terry923</td>
                                    <td class="text-center">3</td>
                                    <td class="text-center">0</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>vexa342</td>
                                    <td class="text-center">3</td>
                                    <td class="text-center">35</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>terry923</td>
                                    <td class="text-center">3</td>
                                    <td class="text-center">0</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>


                                <tr data-level="1">
                                    <td>vg59365</td>
                                    <td class="text-center">2</td>
                                    <td class="text-center">37</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-nowrap"><strong>$1,000.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>vexa342</td>
                                    <td class="text-center">3</td>
                                    <td class="text-center">35</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>terry923</td>
                                    <td class="text-center">3</td>
                                    <td class="text-center">0</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>vexa342</td>
                                    <td class="text-center">3</td>
                                    <td class="text-center">35</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>terry923</td>
                                    <td class="text-center">3</td>
                                    <td class="text-center">0</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>vexa342</td>
                                    <td class="text-center">3</td>
                                    <td class="text-center">35</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-nowrap"><strong>$100.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                                <tr data-level="2">
                                    <td>terry923</td>
                                    <td class="text-center">3</td>
                                    <td class="text-center">0</td>
                                    <td class="text-center">1</td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-nowrap"><strong>$0.00</strong></td>
                                    <td class="text-right"><a href="#">http://vx.ioo/r/VX239499102442</a></td>
                                </tr>
                            </table>
                        </div>

                        <div class="text-center">
                            <a href="#" class="btn btn_sm btn_show_more">Show more</a>
                        </div>

                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>


        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
