<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Page</h1>
                            <div class="heading__time">System time:  <strong>2019-07-16 14:14:48</strong></div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn btn_yellow btn_xs ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/board.inc.php') ?>

                    <div class="row row_xl">
                        <div class="col col-xs-12 col-md-6 col-lg-6 col-gutter-lr">
                            <div class="white_block mb_40">
                                <div class="white_block__heading">
                                    <h4>General</h4>
                                </div>
                                <div class="form_group">
                                    <label class="form_label">General</label>
                                    <div class="form_inline">
                                        <input class="form_inline__input" type="text" name="email" placeholder="mymail@gmail.com" value="">
                                        <button type="button" class="btn btn_yellow btn_xs btn_copy">Change</button>
                                    </div>
                                </div>
                                <div class="form_group">
                                    <label class="form_label">Nickname</label>
                                    <div class="form_inline">
                                        <input class="form_inline__input" type="text" name="email" placeholder="Nickname" value="">
                                        <button type="button" class="btn btn_yellow btn_xs btn_save">Save</button>
                                    </div>
                                </div>
                            </div>
                            <div class="white_block mb_40">
                                <div class="white_block__heading">
                                    <h4>Personal Information</h4>
                                    <span class="property property_valid">Verefied</span>
                                    <span class="property property_process">Processin</span>
                                    <span class="property property_invalid">Unverified</span>
                                </div>
                                <form class="form">
                                    <div class="row row_sm">
                                        <div class="col col-xs-12 col-lg-6 col-gutter-lr">
                                            <div class="form_group">
                                                <div class="form_label">Name</div>
                                                <input type="text" class="form_control" name="n1" placeholder="" value="Andrew">
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-lg-6 col-gutter-lr">
                                            <div class="form_group">
                                                <div class="form_label">Surname</div>
                                                <input type="text" class="form_control" name="n1" placeholder="" value="Nelson">
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-lg-6 col-gutter-lr">
                                            <div class="form_group">
                                                <div class="form_label">Date of birth</div>
                                                <input type="text" class="form_control form_control_calendar" name="n1" placeholder="" value="1993-03-01">
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-lg-6 col-gutter-lr">
                                            <div class="form_group">
                                                <div class="form_label">Document ID</div>
                                                <input type="text" class="form_control" name="n1" placeholder="" value="KR392044">
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-lg-6 col-gutter-lr">
                                            <div class="form_group">
                                                <div class="form_label">Country</div>
                                                <select class="form_control form_select" name="s1">
                                                    <option value="Poland">Poland</option>
                                                    <option value="Russia">Russia</option>
                                                    <option value="Latvia">Latvia</option>
                                                    <option value="USA">USA</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-lg-6 col-gutter-lr">
                                            <div class="form_group">
                                                <div class="form_label">City</div>
                                                <select class="form_control form_select" name="s1">
                                                    <option value="Krakow">Krakow</option>
                                                    <option value="Kiev">Kiev</option>
                                                    <option value="Moscow">Moscow</option>
                                                    <option value="New York">New York</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-lg-12 col-gutter-lr">
                                            <div class="form_group">
                                                <div class="form_label">Address</div>
                                                <input type="text" class="form_control" name="n1" placeholder="" value="Krowodrza 34">
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-lg-6 col-gutter-lr">
                                            <div class="form_group">
                                                <div class="form_label">Zip/Postal Code</div>
                                                <input type="text" class="form_control" name="n1" placeholder="" value="30-007">
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-lg-6 col-gutter-lr">
                                            <div class="form_group">
                                                <div class="form_label">Tax ID</div>
                                                <input type="text" class="form_control" name="n1" placeholder="" value="5667324">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_image">
                                        <div class="form_image__item">
                                            <img src="images/doc.png" class="img-fluid" alt="">
                                        </div>
                                        <ul class="btn_group btn_group_center">
                                            <li>
                                                <button type="button" class="btn btn_yellow btn_xs btn_save">Save</button>
                                            </li>
                                            <li>
                                                <button type="button" class="btn btn_xs">cancel</button>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="form_image__group">
                                        <div class="form_image__thumb">
                                            <i><img src="images/doc_thumb.png" alt=""></i>
                                            <span>passport_scr_3021.jpg</span>
                                            <a href="#" class="form_image__remove"></a>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn_yellow btn_save">SAVE CHANGES</button>
                                </form>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-md-6 col-lg-6 col-gutter-lr">

                            <div class="white_block mb_40">
                                <div class="white_block__heading">
                                    <h4>Cryptocurrency wallets</h4>
                                </div>
                                <div class="form_group">
                                    <label class="form_label">Bitcoin</label>
                                    <div class="form_inline">
                                        <input class="form_inline__input input_btc" type="text" name="email" placeholder="" value="fjgjhhslsjhldiSSjmnbsjwjlq1fhjjszcewF">
                                        <button type="button" class="btn btn_yellow btn_xs btn_copy">Change</button>
                                    </div>
                                </div>
                            </div>

                            <div class="white_block mb_40">
                                <div class="white_block__heading">
                                    <h4>Security & Notifications</h4>
                                </div>
                                <div class="form_group">
                                    <label class="form_switch">
                                        <input type="checkbox" name="switch">
                                        <span>Turn on Google Authenticator</span>
                                    </label>
                                </div>
                                <div class="form_group mb_30">
                                    <label class="form_switch">
                                        <input type="checkbox" name="switch" checked>
                                        <span>Turn on E-mail notifications</span>
                                    </label>
                                </div>
                                <div class="form_box mb_40">
                                    <div class="form_qr">
                                        <img src="images/qr.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="form_group">
                                        <label class="form_label text-center">Enter 2FA code:</label>
                                        <input type="text" class="form_control text-center" name="f4" value="" placeholder="">
                                    </div>
                                    <div class="form_group">
                                        <label class="form_label text-center">Recovery Key:</label>
                                        <input type="text" class="form_control text-center" name="f4" value="S6UUYCGQMGCMXUJF" placeholder="" disabled>
                                    </div>
                                    <div class="text-center mb_20">Important: Print a backup of your recovery key.</div>
                                    <ul class="btn_group btn_group_center">
                                        <li>
                                            <button type="button" class="btn btn_yellow">enable 2fa</button>
                                        </li>
                                        <li>
                                            <button type="button" class="btn">cancel</button>
                                        </li>
                                    </ul>
                                </div>
                                <div class="white_block__heading">
                                    <h4>Change password</h4>
                                </div>
                                <div class="row row_sm mb_20">
                                    <div class="col col-xs-12 col-lg-6 col-gutter-lr">
                                        <div class="form_group">
                                            <div class="form_label">New password</div>
                                            <input type="password" class="form_control" name="n1" placeholder="******" value="">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-lg-6 col-gutter-lr">
                                        <div class="form_group">
                                            <div class="form_label">Repeat new password</div>
                                            <input type="password" class="form_control" name="n1" placeholder="******" value="">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-lg-12 col-gutter-lr">
                                        <div class="form_group">
                                            <div class="form_label">Current password</div>
                                            <input type="password" class="form_control" name="n1" placeholder="******" value="">
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn_yellow btn_lock">change password</button>
                            </div>
                        </div>
                    </div>

                    <div class="row row_xl">
                        <div class="col col-xs-12 col-md-6 col-lg-6 col-gutter-lr">
                            <div class="white_block mb_40">
                                <div class="white_block__heading">
                                    <h4>Themes</h4>
                                </div>
                                <div class="group_inline">
                                    <label class="form_radio">
                                        <input type="radio" name="rr1" value="">
                                        <span>Dark theme</span>
                                    </label>
                                    <label class="form_radio">
                                        <input type="radio" name="rr1" value="" checked>
                                        <span>Light theme</span>
                                    </label>
                                    <label class="form_radio">
                                        <input type="radio" name="rr1" value="">
                                        <span>Combinate theme</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-md-6 col-lg-6 col-gutter-lr">
                            <div class="white_block mb_40">
                                <div class="white_block__heading">
                                    <h4>Subscription</h4>
                                </div>
                                <div class="group_inline">
                                    <label class="form_switch">
                                        <input type="checkbox" name="switch">
                                        <span>News subscription</span>
                                    </label>
                                    <label class="form_switch">
                                        <input type="checkbox" name="switch" checked>
                                        <span> Ticket alert subscription</span>
                                    </label>
                                    <label class="form_switch">
                                        <input type="checkbox" name="switch">
                                        <span>Subscription to letters of payments</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>


        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
