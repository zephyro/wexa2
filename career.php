<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Career</h1>
                            <div class="heading__time">System time:  <strong>2019-07-16 14:14:48</strong></div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn btn_yellow btn_xs ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <div class="career">

                        <!-- 2 -->
                        <div class="level level_blue">
                            <div class="level__inner">

                                <div class="level__header">
                                    <div class="level__step">
                                        <div class="level__step_wrap">
                                            <strong>2</strong>
                                            <span>LEVEL</span>
                                        </div>
                                    </div>

                                    <div class="level__name">
                                        <div class="level__name_wrap">
                                            <span>beginner</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="level__content">

                                    <ul class="level__info">
                                        <li>
                                            <strong>$1,000</strong>
                                            <span>Bonus</span>
                                        </li>
                                        <li>
                                            <strong>$30,000</strong>
                                            <span>Sales</span>
                                        </li>
                                        <li>
                                            <strong>$2000</strong>
                                            <span>Package</span>
                                        </li>
                                        <li>
                                            <strong>10.00%</strong>
                                            <span>Percent</span>
                                        </li>
                                    </ul>

                                </div>

                                <div class="level__progress">
                                    <div class="level__progress_item">
                                        <div class="level__progress_chart">
                                            <div class="level__chart">
                                                <svg width="100%" height="100%" viewBox="0 0 42 42" class="donut">
                                                    <circle class="donut-hole" cx="21" cy="21" r="15.91549430918954" fill="#fff"></circle>
                                                    <circle class="donut-ring" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#0a73ff" stroke-width="2"></circle>
                                                    <circle class="donut-segment" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#efece2" stroke-width="2.5" stroke-dasharray="0 100" stroke-dashoffset="0"></circle>
                                                </svg>
                                            </div>
                                            <div class="level__progress_wrap">
                                                <strong>1st</strong>
                                                <span>LEG</span>
                                            </div>
                                        </div>
                                        <div class="level__progress_value">
                                            <span>100%</span>
                                        </div>
                                    </div>
                                    <div class="level__progress_item">
                                        <div class="level__progress_chart">
                                            <div class="level__chart">
                                                <svg width="100%" height="100%" viewBox="0 0 42 42" class="donut">
                                                    <circle class="donut-hole" cx="21" cy="21" r="15.91549430918954" fill="#fff"></circle>
                                                    <circle class="donut-ring" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#0a73ff" stroke-width="2"></circle>
                                                    <circle class="donut-segment" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#efece2" stroke-width="2.5" stroke-dasharray="0 100" stroke-dashoffset="0"></circle>
                                                </svg>
                                            </div>
                                            <div class="level__progress_wrap">
                                                <strong>2nd</strong>
                                                <span>LEG</span>
                                            </div>
                                        </div>
                                        <div class="level__progress_value">
                                            <span>100%</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- 3 -->
                        <div class="level level_blue">
                            <div class="level__inner">

                                <div class="level__header">
                                    <div class="level__step">
                                        <div class="level__step_wrap">
                                            <strong>3</strong>
                                            <span>LEVEL</span>
                                        </div>
                                    </div>

                                    <div class="level__name">
                                        <div class="level__name_wrap">
                                            <span>junior <br/>agent</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="level__content">

                                    <ul class="level__info">
                                        <li>
                                            <strong>$1,000</strong>
                                            <span>Bonus</span>
                                        </li>
                                        <li>
                                            <strong>$30,000</strong>
                                            <span>Sales</span>
                                        </li>
                                        <li>
                                            <strong>$2000</strong>
                                            <span>Package</span>
                                        </li>
                                        <li>
                                            <strong>10.00%</strong>
                                            <span>Percent</span>
                                        </li>
                                    </ul>

                                </div>

                                <div class="level__progress">
                                    <div class="level__progress_item">
                                        <div class="level__progress_chart">
                                            <div class="level__chart">
                                                <svg width="100%" height="100%" viewBox="0 0 42 42" class="donut">
                                                    <circle class="donut-hole" cx="21" cy="21" r="15.91549430918954" fill="#fff"></circle>
                                                    <circle class="donut-ring" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#0a73ff" stroke-width="2"></circle>
                                                    <circle class="donut-segment" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#efece2" stroke-width="2.5" stroke-dasharray="75 25" stroke-dashoffset="0"></circle>
                                                </svg>
                                            </div>
                                            <div class="level__progress_wrap">
                                                <strong>1st</strong>
                                                <span>LEG</span>
                                            </div>
                                        </div>
                                        <div class="level__progress_value">
                                            <span>100%</span>
                                        </div>
                                    </div>
                                    <div class="level__progress_item">
                                        <div class="level__progress_chart">
                                            <div class="level__chart">
                                                <svg width="100%" height="100%" viewBox="0 0 42 42" class="donut">
                                                    <circle class="donut-hole" cx="21" cy="21" r="15.91549430918954" fill="#fff"></circle>
                                                    <circle class="donut-ring" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#0a73ff" stroke-width="2"></circle>
                                                    <circle class="donut-segment" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#efece2" stroke-width="2.5" stroke-dasharray="0 100" stroke-dashoffset="0"></circle>
                                                </svg>
                                            </div>
                                            <div class="level__progress_wrap">
                                                <strong>2nd</strong>
                                                <span>LEG</span>
                                            </div>
                                        </div>
                                        <div class="level__progress_value">
                                            <span>100%</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- 4 -->
                        <div class="level level_yellow">
                            <div class="level__inner">

                                <div class="level__header">
                                    <div class="level__step">
                                        <div class="level__step_wrap">
                                            <strong>4</strong>
                                            <span>LEVEL</span>
                                        </div>
                                    </div>

                                    <div class="level__name">
                                        <div class="level__name_wrap">
                                            <span>Agent</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="level__content">

                                    <ul class="level__info">
                                        <li>
                                            <strong>$1,000</strong>
                                            <span>Bonus</span>
                                        </li>
                                        <li>
                                            <strong>$30,000</strong>
                                            <span>Sales</span>
                                        </li>
                                        <li>
                                            <strong>$2000</strong>
                                            <span>Package</span>
                                        </li>
                                        <li>
                                            <strong>10.00%</strong>
                                            <span>Percent</span>
                                        </li>
                                    </ul>
                                    <div class="level__present">
                                        <i><img src="img/icon__present.png" alt=""></i>
                                        <span>Bonus or <strong>iPhone</strong></span>
                                    </div>

                                </div>

                                <div class="level__progress">
                                    <div class="level__progress_item">
                                        <div class="level__progress_chart">
                                            <div class="level__chart">
                                                <svg width="100%" height="100%" viewBox="0 0 42 42" class="donut">
                                                    <circle class="donut-hole" cx="21" cy="21" r="15.91549430918954" fill="#fff"></circle>
                                                    <circle class="donut-ring" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#ffcc00" stroke-width="2"></circle>
                                                    <circle class="donut-segment" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#efece2" stroke-width="2.5" stroke-dasharray="80 20" stroke-dashoffset="0"></circle>
                                                </svg>
                                            </div>
                                            <div class="level__progress_wrap">
                                                <strong>1st</strong>
                                                <span>LEG</span>
                                            </div>
                                        </div>
                                        <div class="level__progress_value">
                                            <span>20%</span>
                                        </div>
                                    </div>
                                    <div class="level__progress_item">
                                        <div class="level__progress_chart">
                                            <div class="level__chart">
                                                <svg width="100%" height="100%" viewBox="0 0 42 42" class="donut">
                                                    <circle class="donut-hole" cx="21" cy="21" r="15.91549430918954" fill="#fff"></circle>
                                                    <circle class="donut-ring" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#ffcc00" stroke-width="2"></circle>
                                                    <circle class="donut-segment" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#efece2" stroke-width="2.5" stroke-dasharray="0 100" stroke-dashoffset="0"></circle>
                                                </svg>
                                            </div>
                                            <div class="level__progress_wrap">
                                                <strong>2nd</strong>
                                                <span>LEG</span>
                                            </div>
                                        </div>
                                        <div class="level__progress_value">
                                            <span>75%</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- 5 -->
                        <div class="level level_gray">
                            <div class="level__inner">

                                <div class="level__header">
                                    <div class="level__step">
                                        <div class="level__step_wrap">
                                            <strong>5</strong>
                                            <span>LEVEL</span>
                                        </div>
                                    </div>

                                    <div class="level__name">
                                        <div class="level__name_wrap">
                                            <span>senior <br/>agent</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="level__content">

                                    <ul class="level__info">
                                        <li>
                                            <strong>500</strong>
                                            <span>Bonus</span>
                                        </li>
                                        <li>
                                            <strong>$30,000</strong>
                                            <span>Sales</span>
                                        </li>
                                        <li>
                                            <strong>$2000</strong>
                                            <span>Package</span>
                                        </li>
                                        <li>
                                            <strong>10.00%</strong>
                                            <span>Percent</span>
                                        </li>
                                    </ul>
                                    <div class="level__present">
                                        <i><img src="img/icon__present.png" alt=""></i>
                                        <span>Bonus or <strong>MacBook Pro</strong></span>
                                    </div>

                                </div>

                                <div class="level__progress">
                                    <div class="level__progress_item">
                                        <div class="level__progress_chart">
                                            <div class="level__chart">
                                                <svg width="100%" height="100%" viewBox="0 0 42 42" class="donut">
                                                    <circle class="donut-hole" cx="21" cy="21" r="15.91549430918954" fill="#fff"></circle>
                                                    <circle class="donut-ring" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#0a73ff" stroke-width="2"></circle>
                                                    <circle class="donut-segment" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#efece2" stroke-width="2.5" stroke-dasharray="100 0" stroke-dashoffset="0"></circle>
                                                </svg>
                                            </div>
                                            <div class="level__progress_wrap">
                                                <strong>1st</strong>
                                                <span>LEG</span>
                                            </div>
                                        </div>
                                        <div class="level__progress_value">
                                            <span>0%</span>
                                        </div>
                                    </div>
                                    <div class="level__progress_item">
                                        <div class="level__progress_chart">
                                            <div class="level__chart">
                                                <div class="level__chart">
                                                    <svg width="100%" height="100%" viewBox="0 0 42 42" class="donut">
                                                        <circle class="donut-hole" cx="21" cy="21" r="15.91549430918954" fill="#fff"></circle>
                                                        <circle class="donut-ring" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#0a73ff" stroke-width="2"></circle>
                                                        <circle class="donut-segment" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#efece2" stroke-width="2.5" stroke-dasharray="100 0" stroke-dashoffset="0"></circle>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="level__progress_wrap">
                                                <strong>2nd</strong>
                                                <span>LEG</span>
                                            </div>
                                        </div>
                                        <div class="level__progress_value">
                                            <span>0%</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- 6 -->
                        <div class="level level_gray">
                            <div class="level__inner">

                                <div class="level__header">
                                    <div class="level__step">
                                        <div class="level__step_wrap">
                                            <strong>6</strong>
                                            <span>LEVEL</span>
                                        </div>
                                    </div>

                                    <div class="level__name">
                                        <div class="level__name_wrap">
                                            <span>junior <br/>manager</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="level__content">

                                    <ul class="level__info">
                                        <li>
                                            <strong>500</strong>
                                            <span>Bonus</span>
                                        </li>
                                        <li>
                                            <strong>$30,000</strong>
                                            <span>Sales</span>
                                        </li>
                                        <li>
                                            <strong>$2000</strong>
                                            <span>Package</span>
                                        </li>
                                        <li>
                                            <strong>10.00%</strong>
                                            <span>Percent</span>
                                        </li>
                                    </ul>
                                </div>

                                <div class="level__progress">
                                    <div class="level__progress_item">
                                        <div class="level__progress_chart">
                                            <div class="level__chart">
                                                <div class="level__chart">
                                                    <svg width="100%" height="100%" viewBox="0 0 42 42" class="donut">
                                                        <circle class="donut-hole" cx="21" cy="21" r="15.91549430918954" fill="#fff"></circle>
                                                        <circle class="donut-ring" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#0a73ff" stroke-width="2"></circle>
                                                        <circle class="donut-segment" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#efece2" stroke-width="2.5" stroke-dasharray="100 0" stroke-dashoffset="0"></circle>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="level__progress_wrap">
                                                <strong>1st</strong>
                                                <span>LEG</span>
                                            </div>
                                        </div>
                                        <div class="level__progress_value">
                                            <span>0%</span>
                                        </div>
                                    </div>
                                    <div class="level__progress_item">
                                        <div class="level__progress_chart">
                                            <div class="level__chart">
                                                <div class="level__chart">
                                                    <svg width="100%" height="100%" viewBox="0 0 42 42" class="donut">
                                                        <circle class="donut-hole" cx="21" cy="21" r="15.91549430918954" fill="#fff"></circle>
                                                        <circle class="donut-ring" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#0a73ff" stroke-width="2"></circle>
                                                        <circle class="donut-segment" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#efece2" stroke-width="2.5" stroke-dasharray="100 0" stroke-dashoffset="0"></circle>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="level__progress_wrap">
                                                <strong>2nd</strong>
                                                <span>LEG</span>
                                            </div>
                                        </div>
                                        <div class="level__progress_value">
                                            <span>0%</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- 7 -->
                        <div class="level level_gray">
                            <div class="level__inner">

                                <div class="level__header">
                                    <div class="level__step">
                                        <div class="level__step_wrap">
                                            <strong>7</strong>
                                            <span>LEVEL</span>
                                        </div>
                                    </div>

                                    <div class="level__name">
                                        <div class="level__name_wrap">
                                            <span>manager</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="level__content">

                                    <ul class="level__info">
                                        <li>
                                            <strong>500</strong>
                                            <span>Bonus</span>
                                        </li>
                                        <li>
                                            <strong>$30,000</strong>
                                            <span>Sales</span>
                                        </li>
                                        <li>
                                            <strong>$2000</strong>
                                            <span>Package</span>
                                        </li>
                                        <li>
                                            <strong>10.00%</strong>
                                            <span>Percent</span>
                                        </li>
                                    </ul>
                                </div>

                                <div class="level__progress">
                                    <div class="level__progress_item">
                                        <div class="level__progress_chart">
                                            <div class="level__chart">
                                                <div class="level__chart">
                                                    <svg width="100%" height="100%" viewBox="0 0 42 42" class="donut">
                                                        <circle class="donut-hole" cx="21" cy="21" r="15.91549430918954" fill="#fff"></circle>
                                                        <circle class="donut-ring" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#0a73ff" stroke-width="2"></circle>
                                                        <circle class="donut-segment" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#efece2" stroke-width="2.5" stroke-dasharray="100 0" stroke-dashoffset="0"></circle>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="level__progress_wrap">
                                                <strong>1st</strong>
                                                <span>LEG</span>
                                            </div>
                                        </div>
                                        <div class="level__progress_value">
                                            <span>0%</span>
                                        </div>
                                    </div>
                                    <div class="level__progress_item">
                                        <div class="level__progress_chart">
                                            <div class="level__chart">
                                                <div class="level__chart">
                                                    <svg width="100%" height="100%" viewBox="0 0 42 42" class="donut">
                                                        <circle class="donut-hole" cx="21" cy="21" r="15.91549430918954" fill="#fff"></circle>
                                                        <circle class="donut-ring" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#0a73ff" stroke-width="2"></circle>
                                                        <circle class="donut-segment" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#efece2" stroke-width="2.5" stroke-dasharray="100 0" stroke-dashoffset="0"></circle>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="level__progress_wrap">
                                                <strong>2nd</strong>
                                                <span>LEG</span>
                                            </div>
                                        </div>
                                        <div class="level__progress_value">
                                            <span>0%</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- 14 -->
                        <div class="level level_gray">
                            <div class="level__inner">

                                <div class="level__header">
                                    <div class="level__step">
                                        <div class="level__step_wrap">
                                            <strong>14</strong>
                                            <span>LEVEL</span>
                                        </div>
                                    </div>

                                    <div class="level__name">
                                        <div class="level__name_wrap">
                                            <span>general <br/>president</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="level__content">

                                    <ul class="level__info">
                                        <li>
                                            <strong>$400,000</strong>
                                            <span>Bonus</span>
                                        </li>
                                        <li>
                                            <strong>$100,000,000</strong>
                                            <span>Sales</span>
                                        </li>
                                        <li>
                                            <strong>$50,000</strong>
                                            <span>Package</span>
                                        </li>
                                        <li>
                                            <strong>18.00%</strong>
                                            <span>Percent</span>
                                        </li>
                                    </ul>

                                    <div class="level__present">
                                        <i><img src="img/icon__present.png" alt=""></i>
                                        <span>Bonus or <strong>MacBook Pro</strong></span>
                                    </div>

                                </div>

                                <div class="level__progress">
                                    <div class="level__progress_item">
                                        <div class="level__progress_chart">
                                            <div class="level__chart">
                                                <div class="level__chart">
                                                    <svg width="100%" height="100%" viewBox="0 0 42 42" class="donut">
                                                        <circle class="donut-hole" cx="21" cy="21" r="15.91549430918954" fill="#fff"></circle>
                                                        <circle class="donut-ring" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#0a73ff" stroke-width="2"></circle>
                                                        <circle class="donut-segment" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#efece2" stroke-width="2.5" stroke-dasharray="100 0" stroke-dashoffset="0"></circle>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="level__progress_wrap">
                                                <strong>1st</strong>
                                                <span>LEG</span>
                                            </div>
                                        </div>
                                        <div class="level__progress_value">
                                            <span>0%</span>
                                        </div>
                                    </div>
                                    <div class="level__progress_item">
                                        <div class="level__progress_chart">
                                            <div class="level__chart">
                                                <div class="level__chart">
                                                    <svg width="100%" height="100%" viewBox="0 0 42 42" class="donut">
                                                        <circle class="donut-hole" cx="21" cy="21" r="15.91549430918954" fill="#fff"></circle>
                                                        <circle class="donut-ring" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#0a73ff" stroke-width="2"></circle>
                                                        <circle class="donut-segment" cx="21" cy="21" r="15.91549430918954" fill="transparent" stroke="#efece2" stroke-width="2.5" stroke-dasharray="100 0" stroke-dashoffset="0"></circle>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="level__progress_wrap">
                                                <strong>2nd</strong>
                                                <span>LEG</span>
                                            </div>
                                        </div>
                                        <div class="level__progress_value">
                                            <span>0%</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>


        <?php include('inc/scripts.inc.php') ?>




    </body>
</html>
