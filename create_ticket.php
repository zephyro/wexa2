<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Tickets</h1>
                            <div class="heading__time">System time:  <strong>2019-07-16 14:14:48</strong></div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn btn_yellow btn_xs ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/board.inc.php') ?>

                    <div class="content">

                        <div class="content__header">
                            <div class="content__header_title">
                                <h2>Create ticket</h2>
                            </div>
                        </div>

                        <form class="form">
                            <div class="row">
                                <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                    <div class="form_group">
                                        <label class="form_label">Ticket category</label>
                                        <select class="form_control form_select" name="s1">
                                            <option value="Payments">Payments</option>
                                            <option value="Payments">Payments</option>
                                            <option value="Payments">Payments</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                    <div class="form_group">
                                        <label class="form_label">Subject</label>
                                        <select class="form_control form_select" name="s1">
                                            <option value="English">English</option>
                                            <option value="Russian">Russian</option>
                                            <option value="Spain">Spain</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form_group">
                                <label class="form_label">Message</label>
                                <textarea class="form_control" name="m1" placeholder="" rows="10"></textarea>
                            </div>
                            <div class="form_group">
                                <label class="form_label">Add attachment</label>
                                <div class="file_group">
                                    <label class="file_form">
                                        <input type="file" name="file">
                                        <span class="btn btn_green btn_file">Browse file</span>
                                    </label>
                                    <div class="file_text">
                                        (max 1 file, size 10 MB)<br/>
                                        jpg, gif, png
                                    </div>
                                    <div class="file_group__source">
                                        <div class="file_group__label">Uploaded</div>
                                        <div class="file_group__item">
                                            <span>Bitcoin-Symbol.png </span>
                                            <i class="file_group__remove"></i>
                                        </div>
                                        <div class="file_group__item">
                                            <span>photo_2019-08-15_19-27-49.jpeg</span>
                                            <i class="file_group__remove"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <ul class="btn_group">
                                <li>
                                    <button type="submit" class="btn btn_yellow">Create new ticket</button>
                                </li>
                                <li>
                                    <button type="reset" class="btn">cancel</button>
                                </li>
                            </ul>

                        </form>

                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>


        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
