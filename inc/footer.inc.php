<footer class="footer">
    <div class="container">
        <div class="footer__row">
            <div class="footer__left">
                <div class="footer__text">2019 Vexaglobal.com. All rights reserved.</div>
                <ul class="footer__nav">
                    <li><a href="#">Terms and Conditions</a></li>
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Support</a></li>
                </ul>
            </div>
            <div class="footer__right">
                <div class="lng">
                    <div class="lng__active">
                        <b><img src="img/lng__en.png" class="img-fluid" alt=""></b>
                        <span>english</span>
                    </div>
                    <ul class="lng__dropdown">
                        <li>
                            <a href="#">
                                <i><img src="img/lng__en.png" class="img-fluid" alt=""></i>
                                <span>english</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i><img src="img/lng__ru.png" class="img-fluid" alt=""></i>
                                <span>русский</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i><img src="img/lng__de.png" class="img-fluid" alt=""></i>
                                <span>deutsch</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i><img src="img/lng__pl.png" class="img-fluid" alt=""></i>
                                <span>poland</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>


<!-- Modal -->

<div class="hide">
    <a href="#modal_one" class="open_modal_one btn_modal"></a>
    <div class="modal" id="modal_one">
        <div class="modal__title">Dear Partners!</div>
        <p>
            We are happy to inform you that we have opened the
            opportunity to buy  tickets for our Winter Trading Camp in
            Thailand from November 29, 2019 to December 2, 2019!
            The Winter Trading Camp in Thailand will be held in Hua
            Hin 5* Resort. We are happy to inform you that we have
            opened the opportunity to buy  tickets for our Winter
            Trading Camp <a href="#">in Thailand</a>.
        </p>
        <p class="color_gray">
            We are happy to inform you that we have opened the
            opportunity to buy  tickets for our Winter Trading Camp in
            Thailand from November 29, 2019 to December 2, 2019!
            The Winter Trading Camp.
        </p>
        <div class="text-center mb_20">
            <a href="#" class="btn btn_yellow btn_long_md">Buy TickeTS</a>
        </div>
        <div class="modal__image">
            <img src="images/modal__image.jpg" class="img-fluid" alt="">
        </div>
    </div>
</div>