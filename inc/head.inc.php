<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title></title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic" rel="stylesheet">

<link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">
<link rel="stylesheet" href="js/vendor/mCustomScrollbar/jquery.mCustomScrollbar.css">
<link rel="stylesheet" href="js/vendor/ion.rangeSlider/css/ion.rangeSlider.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/css/swiper.min.css">
<link rel="stylesheet" href="css/main.css">
