<div class="board">
    <div class="board__left">
        <div class="board_item">
            <div class="board_item__image">
                <div class="board_item__icon">
                    <img src="img/board__icon_01.png" class="img-fluid" alt="">
                </div>
            </div>
            <div class="board_item__title">MAIN BALANCE</div>
            <div class="board_item__value">$ 259.50</div>
        </div>
        <div class="board_item">
            <div class="board_item__image">
                <div class="board_item__icon">
                    <img src="img/board__icon_02.png" class="img-fluid" alt="">
                </div>
            </div>
            <div class="board_item__title">FUND BALANCE</div>
            <div class="board_item__value">$ 10.00</div>
        </div>
        <div class="board_item">
            <div class="board_item__image">
                <div class="board_item__icon">
                    <img src="img/board__icon_03.png" class="img-fluid" alt="">
                </div>
            </div>
            <div class="board_item__title">AFFILIATE BALANCE</div>
            <div class="board_item__value">$ 330.75</div>
        </div>
    </div>
    <div class="board__center">
        <div class="board_info">
            <div class="board_info__group">
                <div class="board_info__item board_info__item_01">
                    <span>BASIC</span>
                    <strong>$ 120.00</strong>
                </div>
                <div class="board_info__item board_info__item_02">
                    <span>EXCLUSIVE</span>
                    <strong>$ 0.75</strong>
                </div>
            </div>
            <div class="board_info__group">
                <div class="board_info__item board_info__item_03">
                    <span>PREMIUM</span>
                    <strong>$ 60.00</strong>
                </div>
                <div class="board_info__item board_info__item_04">
                    <span>POINTS</span>
                    <strong>$ 1,250.75</strong>
                </div>
            </div>
        </div>
    </div>
    <div class="board__right">
        <div class="board_item">
            <div class="board_item__image">
                <div class="board_item__icon">
                    <img src="img/board__icon_04.png" class="img-fluid" alt="">
                </div>
                <div class="board_item__bg"></div>
            </div>
            <div class="board_item__title">MY PACKAGES</div>
            <div class="board_item__value">$ 310.00</div>
        </div>
        <div class="board_item">
            <div class="board_item__image">
                <div class="board_item__icon">
                    <img src="img/board__icon_05.png" class="img-fluid" alt="">
                </div>
                <div class="board_item__bg"></div>
            </div>
            <div class="board_item__title">total Sales</div>
            <div class="board_item__value">$ 3,456.50</div>
        </div>
        <div class="board_item">
            <div class="board_item__image">
                <div class="board_item__icon">
                    <img src="img/board__icon_06.png" class="img-fluid" alt="">
                </div>
                <div class="board_item__bg"></div>
            </div>
            <div class="board_item__title">Affiliate income</div>
            <div class="board_item__value">$ 1,528.00</div>
        </div>
    </div>
</div>