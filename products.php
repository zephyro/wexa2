<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Products</h1>
                            <div class="heading__time">System time:  <strong>2019-07-16 14:14:48</strong></div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn btn_yellow btn_xs ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/board.inc.php') ?>

                    <div class="row">
                        <div class="col col-xs-12 col-md-6 col-lg-4 col-gutter-lr mb_30">
                            <div class="white_box">
                                <h2 class="mb_20">Landing page</h2>
                                <div class="product_image mb_25">
                                    <img src="images/product_image_01.jpg" class="img-fluid" alt="">
                                </div>
                                <form class="form">
                                    <div class="form_group mb_35">
                                        <label class="form_label">URL</label>
                                        <input type="text" class="form_control" name="n1" placeholder="" value="http://vx.ioo/landing/VX39381/239399293993hjf8911">
                                    </div>
                                    <button type="button" class="btn btn_yellow btn_save">Save</button>
                                </form>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-md-6 col-lg-4 col-gutter-lr mb_30">
                            <div class="white_box">
                                <h2 class="mb_20">Photo</h2>
                                <div class="product_image mb_40">
                                    <img src="images/product_image_02.jpg" class="img-fluid" alt="">
                                </div>
                                <form class="form">
                                    <div class="file_group mb_40">
                                        <label class="file_form">
                                            <input type="file" name="file">
                                            <span class="btn btn_green btn_file">Browse file</span>
                                        </label>
                                        <div class="file_text">
                                            (max 1 file, size 10 MB)<br>
                                            jpg, gif, png
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn_yellow btn_save">Save</button>
                                </form>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-md-6 col-lg-4 col-gutter-lr mb_30">
                            <div class="white_box">
                                <h2 class="mb_20">Contacts</h2>
                                <form class="form">
                                    <div class="form_group">
                                        <label class="form_label">Facebook</label>
                                        <input type="text" class="form_control form_control_sm" name="n1" placeholder="" value="https://facebook.com/vegaglobal9332">
                                    </div>
                                    <div class="form_group">
                                        <label class="form_label">Instagram</label>
                                        <input type="text" class="form_control form_control_sm" name="n1" placeholder="" value="https://Instagram.com/vegaglobal9332">
                                    </div>
                                    <div class="row">
                                        <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                            <div class="form_group">
                                                <label class="form_label">Telegram</label>
                                                <input type="text" class="form_control form_control_sm" name="n1" placeholder="" value="@vexa9332">
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                            <div class="form_group">
                                                <label class="form_label">WhatsApp</label>
                                                <input type="text" class="form_control form_control_sm" name="n1" placeholder="" value="+7 398 488-33-22">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_group">
                                        <label class="form_label">WeChat</label>
                                        <input type="text" class="form_control form_control_sm" name="n1" placeholder="" value="+7 398 488-33-22">
                                    </div>
                                    <div class="form_group">
                                        <label class="form_label">Who am I?</label>
                                        <textarea class="form_control form_control_sm" name="n1" placeholder="" rows="1"></textarea>
                                    </div>
                                    <div class="form_group mb_15">
                                        <label class="form_label">Welcome message</label>
                                        <textarea class="form_control form_control_sm" name="n1" placeholder="" rows="1"></textarea>
                                    </div>
                                    <button type="button" class="btn btn_yellow btn_save">Save</button>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="content">
                        <div class="content__header">
                            <div class="content__header_title">
                                <h4>Packages list</h4>
                            </div>
                        </div>
                        <div class="content__table">
                            <div class="table_responsive">
                                <table class="table">
                                    <tr>
                                        <th>Period</th>
                                        <th>Packages</th>
                                        <th>AFF. Inc.</th>
                                        <th class="text-nowrap text-right">Download</th>
                                    </tr>

                                    <tr>
                                        <td class="text-nowrap">2019-08</td>
                                        <td class="text-nowrap">0.00</td>
                                        <td class="text-nowrap">0.00</td>
                                        <td class="text-nowrap text-right">
                                            <a href="#" class="download_link">Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">2019-08</td>
                                        <td class="text-nowrap">8.00</td>
                                        <td class="text-nowrap">11.00</td>
                                        <td class="text-nowrap text-right">
                                            <a href="#" class="download_link">Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">2019-08</td>
                                        <td class="text-nowrap">10.00</td>
                                        <td class="text-nowrap">5.00</td>
                                        <td class="text-nowrap text-right">
                                            <a href="#" class="download_link">Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">2019-08</td>
                                        <td class="text-nowrap">0.00</td>
                                        <td class="text-nowrap">1.00</td>
                                        <td class="text-nowrap text-right">
                                            <a href="#" class="download_link">Download</a>
                                        </td>
                                    </tr>
                                 </table>
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>


        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
