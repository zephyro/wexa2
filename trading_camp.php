<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Trading Camp</h1>
                            <div class="heading__time">System time:  <strong>2019-07-16 14:14:48</strong></div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn btn_yellow btn_xs ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/board.inc.php') ?>

                    <div class="row row_xl">
                        <div class="col col-xs-12 col-lg-8 col-gutter-lr mb_40">
                            <div class="white_box">
                                <div class="camp">
                                    <div class="row mb_20">
                                        <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                            <h4>We offer to visit Trading Camp only for 100 partners.</h4>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-gutter-lr">

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                            <div class="camp_image mb_15">
                                                <img src="images/image_01.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                            <p>
                                                We are happy to inform you that we have opened the opportunity to buy
                                                tickets for our Winter Trading Camp in Thailand from November 29, 2019 to
                                                December 2, 2019! The Winter Trading Camp in Thailand will be held in Hua
                                                Hin 5* Resort.
                                            </p>
                                            <ul class="ml_30">
                                                <li><strong>Day 1</strong> - Overview, Rudiments, Principles of Markets vs Theories;</li>
                                                <li><strong>Day 2</strong> - Developing a Regiment, Reference Framework: Moving Averages;</li>
                                                <li><strong>Day 3</strong> - Risk of Loss & Expectation of Profit;</li>
                                                <li><strong>Day 4</strong> - Portfolio & Money Management.</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p><strong>Additionally - we have prepared for you a lot of attractions and amenities!</strong></p>
                                    <ul class="camp__meta">
                                        <li>Traditional Thai & International meals every day</li>
                                        <li>Daily massage</li>
                                        <li>Optional yoga and meditation sessions </li>
                                        <li>Island cruise: Snorkeling & Jet Ski;</li>
                                    </ul>
                                    <div class="camp__footnote">
                                        <span><sup class="color_red">*</sup> and much more!</span>
                                        <span><sup class="color_red">**</sup> plane tickets are not included in the price.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-lg-4 col-gutter-lr mb_40">
                            <div class="white_box">
                                <h2 class="mb_20">Order form</h2>
                                <form class="form">
                                    <div class="form_group">
                                        <label class="form_label">Select balance</label>
                                        <select class="form_control form_select" name="s2">
                                            <option value="Main balance">Main balance</option>
                                            <option value="Main balance">Main balance</option>
                                            <option value="Main balance">Main balance</option>
                                        </select>
                                    </div>
                                    <div class="form_group">
                                        <label class="form_label">Tickets count</label>
                                        <select class="form_control form_select" name="s2">
                                            <option value="949">949</option>
                                            <option value="1000">1000</option>
                                            <option value="810">810</option>
                                        </select>
                                    </div>
                                    <div class="form_group mb_30">
                                        <label class="form_label">Total price, USD</label>
                                        <select class="form_control form_select" name="s2">
                                            <option value="1">1</option>
                                            <option value=2"">2</option>
                                            <option value="3">3</option>
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn_yellow btn_long_md">Order</button>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="content">
                        <div class="content__header">
                            <div class="content__header_title">
                                <h4>Your tickets</h4>
                            </div>
                        </div>
                        <div class="content__table mb_20">
                            <div class="table_responsive">
                                <table class="table">
                                    <tr>
                                        <th>Date</th>
                                        <th>Created</th>
                                        <th class="text-nowrap text-right">Download</th>
                                    </tr>

                                    <tr>
                                        <td class="text-nowrap">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap">1</td>
                                        <td class="text-nowrap text-right">
                                            <a href="#" class="download_link">Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-nowrap">22 June 2019, at 13:45</td>
                                        <td class="text-nowrap">1</td>
                                        <td class="text-nowrap text-right">
                                            <a href="#" class="download_link">Download</a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="text-center">
                            <a href="#" class="btn btn_show_more">Show more</a>
                        </div>
                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>


        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
