<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Buy packages</h1>
                            <div class="heading__time">System time:  <strong>2019-07-16 14:14:48</strong></div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn btn_yellow btn_xs ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/board.inc.php') ?>


                    <div class="pack_block">

                        <div class="pack_group">
                            <div class="row">
                                <div class="col col-xs-12 col-sm-4 col-lg-4 col-gutter-lr">
                                    <div class="pack active" data-name="basic" data-days="130">
                                        <div class="pack__icon">
                                            <div class="pack__icon_base">
                                                <img src="img/pack__01_base.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="pack__icon_hover">
                                                <img src="img/pack__01_hover.png" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="pack__content">
                                            <div class="pack__content_title">Basic</div>
                                            <div class="pack__content_value">$50 — $999</div>
                                            <div class="pack__content_time">130 business days</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-xs-12 col-sm-4 col-lg-4 col-gutter-lr">
                                    <div class="pack" data-name="premium" data-days="150">
                                        <div class="pack__icon">
                                            <div class="pack__icon_base">
                                                <img src="img/pack__02_base.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="pack__icon_hover">
                                                <img src="img/pack__02_hover.png" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="pack__content">
                                            <div class="pack__content_title">premium</div>
                                            <div class="pack__content_value">$1,000 — $9,999</div>
                                            <div class="pack__content_time">150 business days</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-xs-12 col-sm-4 col-lg-4 col-gutter-lr">
                                    <div class="pack" data-name="exclusive" data-days="180">
                                        <div class="pack__icon">
                                            <div class="pack__icon_base">
                                                <img src="img/pack__03_base.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="pack__icon_hover">
                                                <img src="img/pack__03_hover.png" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="pack__content">
                                            <div class="pack__content_title">exclusive</div>
                                            <div class="pack__content_value">$10,000 — $100,000</div>
                                            <div class="pack__content_time">180 business days</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="content">
                            <div class="pack_block__header">
                                <div class="pack_block__title">VEXAGLOBAL.<span class="pack_block_name">BASIC</span></div>
                                <div class="pack_block__time"><span class="pack_block_day">130</span> business days</div>
                            </div>
                            <form class="form">
                                <input type="hidden" name="pack_block_name" value="BASIC">
                                <input type="hidden" name=pack_block_day"" value="130">
                                <div class="pack_block__wrap">
                                    <div class="row">
                                        <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                            <div class="form_group">
                                                <label class="form_label">Select balance</label>
                                                <select class="form_control form_select" name="pack_balance">
                                                    <option value="Main balance">Main balance</option>
                                                    <option value="Main balance">Main balance</option>
                                                    <option value="Main balance">Main balance</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                            <div class="form_group">
                                                <label class="form_label">Amount</label>
                                                <select class="form_control form_select" name="pack_amount">
                                                    <option value="540">$540.00</option>
                                                    <option value="1000">$1000.00</option>
                                                    <option value="2000">$2000.00</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                            <div class="form_group">
                                                <label class="form_label">Daily profit</label>
                                                <input type="text" class="form_control" name="pack_profit" placeholder="" value="$5.25" disabled>
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                            <div class="form_group">
                                                <label class="form_label">Total return</label>
                                                <input type="text" class="form_control" name="pack_return" placeholder="" value="$681.85" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="pack_range">
                                    <div class="pack_range__grid pack_range__start">$50</div>
                                    <div class="pack_range__slider">
                                        <input type="hidden" id="pack_slider" name="pack_slider" value=""
                                               data-type="single"
                                               data-min="0"
                                               data-max="999"
                                               data-from="400"
                                               data-grid="false"
                                        />
                                    </div>
                                    <div class="pack_range__grid pack_range__finish">$999</div>
                                </div>

                                <div class="text-center">
                                    <button class="btn btn_yellow" type="submit">buy package</button>
                                </div>
                            </form>

                        </div>

                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>


        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
