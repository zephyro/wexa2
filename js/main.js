// Nav

(function() {

    $('.nav_toggle').on('click', function(e){
        e.preventDefault();
        $('.page').toggleClass('nav_open');
    });

}());


// lng
(function() {

    $('.lng__active').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.lng').addClass('open');
    });


    $('.lng__dropdown a').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.lng').removeClass('open');
    });

}());

// Hide dropdown

$('body').click(function (event) {

    if ($(event.target).closest(".lng").length === 0) {
        $(".lng").removeClass('open');
    }

    if ($(event.target).closest(".currency").length === 0) {
        $(".currency").removeClass('open');
    }

    if ($(event.target).closest(".form_lng").length === 0) {
        $(".form_lng").removeClass('open');
    }

});



$(".btn_modal").fancybox({
    'padding'    : 0
});

$(".informer__notice_scroll").mCustomScrollbar({
    theme:"minimal-dark"
});

$(".table_box__scroll").mCustomScrollbar({
    theme:"minimal-dark"
});



// Navigation
(function() {

    $(".nav_item")
        .mouseenter(function() {
            var point = $(this).attr("data-nav");
            var nav = '.nav_' + point;
            var bg = 'nav_item_' + point;
            $('.header_nav').find('.nav_item').removeClass('hover');
            $('.header_nav').find(nav).addClass('hover');
            $('.header_nav__hover').removeClass('nav_item_one').removeClass('nav_item_two').removeClass('nav_item_three').removeClass('nav_item_four');
            $('.header_nav__hover').addClass(bg);
        })
        .mouseleave(function(){
            $('.header_nav').find('.nav_item').delay(500).removeClass('hover');
            $('.header_nav__hover').removeClass('nav_item_01').removeClass('nav_item_02').removeClass('nav_item_03').removeClass('nav_item_04');
        });

}());


// Tabs

(function() {

    $('.tabs_nav a').on('click touchstart', function(e){
        e.preventDefault();

        var tab = $($(this).attr("data-target"));
        var box = $(this).closest('.tabs');

        $(this).closest('.tabs_nav').find('a').removeClass('active');
        $(this).addClass('active');

        box.find('.tabs_item').removeClass('active');
        box.find(tab).addClass('active');
    });

}());



(function() {

    $('.currency__active').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.currency').toggleClass('open');
    });

    $('.currency__item').on('click touchstart', function(e){
        e.preventDefault();
        var box = $(this).closest('.currency');
        var icon = $(this).find('img').attr('src');
        var name = $(this).find('.currency__item_name').text();

        box.find('.currency__active_icon img').attr('src', icon);
        box.find('.currency__active_text span').text(name);

        box.removeClass('open');
    });

}());


(function() {

    $('.pack').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.pack_group').find('.pack').removeClass('active');
        $(this).addClass('active');
    });

    // Filter slider
    var $pack = $("#pack_slider");

    $pack.ionRangeSlider({
        skin: "round"
    });


}());


(function() {
    $('.table_filter a').on('click touchstart', function(e){
        e.preventDefault();
        $(this).toggleClass('active');
    });
}());


// form lng
(function() {

    $('.form_lng__active').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.form_lng').addClass('open');
    });

    $('.form_lng__dropdown li').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.form_lng').removeClass('open');
        var lngImage = $(this).find('img').attr('src');
        var lngName = $(this).attr('data-value');
        var lngActive = $(this).closest('.form_lng').find('.form_lng__active');
        lngActive.find('img').attr('src',lngImage);
        lngActive.find('span').text(lngName);
        $(this).closest('.form_lng').find('.form_lng__input').val(lngName);
    });

}());


var bnr = new Swiper('.bnr_slider', {
    slidesPerView: 4,
    spaceBetween: 40,
    loop: true,
    navigation: {
        nextEl: '.bnr_next',
        prevEl: '.bnr_prev',
    },
    breakpoints: {
        1200: {
            slidesPerView: 2,
            spaceBetween: 30
        },
        992: {
            slidesPerView: 2,
            spaceBetween: 30
        },
        768: {
            slidesPerView: 1,
            spaceBetween: 30
        }
    }
});

var brand = new Swiper('.brand', {
    slidesPerView: 4,
    spaceBetween: 40,
    loop: true,
    navigation: {
        nextEl: '.brand_next',
        prevEl: '.brand_prev',
    },
    breakpoints: {
        1200: {
            slidesPerView: 2,
            spaceBetween: 30
        },
        992: {
            slidesPerView: 2,
            spaceBetween: 30
        },
        768: {
            slidesPerView: 1,
            spaceBetween: 30
        }
    }
});

(function() {
    $('.collapse__header_toggle').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.collapse').find('.collapse__content').slideToggle('fast');
        $(this).closest('.collapse').toggleClass('close');
    });
}());


(function() {

    $('.tree_table').treeTable({
        ignoreClickOn: "input, a, img"
    });

}());

