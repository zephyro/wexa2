<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Page</h1>
                            <div class="heading__time">System time:  <strong>2019-07-16 14:14:48</strong></div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn btn_yellow btn_xs ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/board.inc.php') ?>

                    <div class="row row_xl">
                        <div class="col col-xs-12 col-lg-4 col-gutter-lr mb_40">
                            <div class="collapse">
                                <div class="collapse__header">
                                    <div class="collapse__header_title">Presentations</div>
                                    <span class="collapse__header_toggle"></span>
                                </div>
                                <div class="collapse__content">
                                    <div class="form_group mb_40">
                                        <label class="form_label">Select language</label>
                                        <div class="form_lng">
                                            <input class="form_lng__input" type="hidden" value="english">
                                            <div class="form_lng__active">
                                                <i><img src="img/lng__en.png" class="img-fluid" alt=""></i>
                                                <span>English</span>
                                            </div>
                                            <ul class="form_lng__dropdown">
                                                <li data-value="english">
                                                    <i><img src="img/lng__en.png" class="img-fluid" alt=""></i>
                                                    <span>English</span>
                                                </li>
                                                <li data-value="deutsch">
                                                    <i><img src="img/lng__de.png" class="img-fluid" alt=""></i>
                                                    <span>Deutsch</span>
                                                </li>
                                                <li data-value="русский">
                                                    <i><img src="img/lng__ru.png" class="img-fluid" alt=""></i>
                                                    <span>Русский</span>
                                                </li>
                                                <li data-value="poland">
                                                    <i><img src="img/lng__pl.png" class="img-fluid" alt=""></i>
                                                    <span>Poland</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="collapse__content_line"></div>
                                    <ul class="present_actions">
                                        <li>
                                            <div class="present_actions__title">Presentation PDF</div>
                                            <a href="#" class="btn btn_gray btn_sm btn_download">
                                                <i><img src="img/icon__download_black.png" alt=""></i>
                                                <span>Download</span>
                                            </a>
                                        </li>
                                        <li>
                                            <div class="present_actions__title">Marketing plan PDF</div>
                                            <a href="#" class="btn btn_gray btn_sm btn_download">
                                                <i><img src="img/icon__download_black.png" alt=""></i>
                                                <span>Download</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-lg-8 col-gutter-lr mb_40">
                            <div class="collapse">
                                <div class="collapse__header">
                                    <div class="collapse__header_title">Bonus Programs</div>
                                    <span class="collapse__header_toggle"></span>
                                </div>
                                <div class="collapse__content">
                                    <div class="row">
                                        <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                            <div class="collapse__bonus">
                                                <div class="collapse__bonus_image">
                                                    <img src="images/bonus_image__01.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="text-center">
                                                    <a href="#" class="btn btn_yellow">Cars</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                            <div class="collapse__bonus">
                                                <div class="collapse__bonus_image">
                                                    <img src="images/bonus_image__02.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="text-center">
                                                    <a href="#" class="btn btn_yellow">Cars</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="collapse mb_40">
                        <div class="collapse__header">
                            <div class="collapse__header_title">Banners</div>
                            <span class="collapse__header_toggle"></span>
                            <div class="collapse__header_nav">
                                <div class="collapse__nav collapse__nav_prev bnr_prev"></div>
                                <div class="collapse__nav collapse__nav_next bnr_next"></div>
                            </div>
                        </div>
                        <div class="collapse__content">
                            <div class="bnr_slider swiper-container">
                                <div class="swiper-wrapper">

                                    <div class="swiper-slide">
                                        <div class="bnr_item">
                                            <a class="bnr_item__image" href="images/bnr__01.jpg" data-fancybox="bnr">
                                                <img src="images/bnr__01.jpg" class="img-fluid" alt="">
                                            </a>
                                            <div class="bnr_item__title">Unlimited solutions</div>
                                            <div class="text-center">
                                                <a href="#" class="btn btn_gray btn_md btn_download">
                                                    <i><img src="img/icon__download_black.png" alt=""></i>
                                                    <span>Download</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="bnr_item">
                                            <a class="bnr_item__image" href="images/bnr__02.jpg" data-fancybox="bnr">
                                                <img src="images/bnr__02.jpg" class="img-fluid" alt="">
                                            </a>
                                            <div class="bnr_item__title">You release choise</div>
                                            <div class="text-center">
                                                <a href="#" class="btn btn_gray btn_md btn_download">
                                                    <i><img src="img/icon__download_black.png" alt=""></i>
                                                    <span>Download</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="bnr_item">
                                            <a class="bnr_item__image" href="images/bnr__03.jpg" data-fancybox="bnr">
                                                <img src="images/bnr__03.jpg" class="img-fluid" alt="">
                                            </a>
                                            <div class="bnr_item__title">Get maximum benifits</div>
                                            <div class="text-center">
                                                <a href="#" class="btn btn_gray btn_md btn_download">
                                                    <i><img src="img/icon__download_black.png" alt=""></i>
                                                    <span>Download</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="bnr_item">
                                            <a class="bnr_item__image" href="images/bnr__04.jpg" data-fancybox="bnr">
                                                <img src="images/bnr__04.jpg" class="img-fluid" alt="">
                                            </a>
                                            <div class="bnr_item__title">Make money online</div>
                                            <div class="text-center">
                                                <a href="#" class="btn btn_gray btn_md btn_download">
                                                    <i><img src="img/icon__download_black.png" alt=""></i>
                                                    <span>Download</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="collapse__content_nav">
                                <div class="collapse__nav collapse__nav_prev bnr_prev"></div>
                                <div class="collapse__nav collapse__nav_next bnr_next"></div>
                            </div>
                        </div>
                    </div>

                    <div class="collapse">
                        <div class="collapse__header">
                            <div class="collapse__header_title">Vexa Global Brand</div>
                            <span class="collapse__header_toggle"></span>
                            <div class="collapse__header_nav">
                                <div class="collapse__nav collapse__nav_prev brand_prev"></div>
                                <div class="collapse__nav collapse__nav_next brand_next"></div>
                            </div>
                        </div>
                        <div class="collapse__content">
                            <div class="brand swiper-container">
                                <div class="swiper-wrapper">

                                    <div class="swiper-slide">
                                        <div class="brand_item">
                                            <a class="brand_item__image" href="images/brand_01.jpg" data-fancybox="bnr">
                                                <img src="images/brand_01.jpg" class="img-fluid" alt="">
                                            </a>
                                            <div class="brand_item__title">Flag blue</div>
                                            <div class="text-center">
                                                <a href="#" class="btn btn_border btn_md">Open source</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="brand_item">
                                            <a class="brand_item__image" href="images/brand_02.jpg" data-fancybox="bnr">
                                                <img src="images/brand_02.jpg" class="img-fluid" alt="">
                                            </a>
                                            <div class="brand_item__title">Notebook 2</div>
                                            <div class="text-center">
                                                <a href="#" class="btn btn_border btn_md">Open source</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="brand_item">
                                            <a class="brand_item__image" href="images/brand_03.jpg" data-fancybox="bnr">
                                                <img src="images/brand_03.jpg" class="img-fluid" alt="">
                                            </a>
                                            <div class="brand_item__title">Mug Artwork 1</div>
                                            <div class="text-center">
                                                <a href="#" class="btn btn_border btn_md">Open source</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="brand_item">
                                            <a class="brand_item__image" href="images/brand_04.jpg" data-fancybox="bnr">
                                                <img src="images/brand_04.jpg" class="img-fluid" alt="">
                                            </a>
                                            <div class="brand_item__title">T-shirt</div>
                                            <div class="text-center">
                                                <a href="#" class="btn btn_border btn_md">Open source</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="collapse__content_nav">
                                <div class="collapse__nav collapse__nav_prev brand_prev"></div>
                                <div class="collapse__nav collapse__nav_next brand_next"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>


        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
