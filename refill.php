<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Refill</h1>
                            <div class="heading__time">System time:  <strong>2019-07-16 14:14:48</strong></div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn btn_yellow btn_xs ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <div class="content">

                        <div class="row pt_10">
                            <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                <div class="qr">
                                    <div class="qr__wrap">
                                        <div class="qr__item">
                                            <img src="images/qr.png" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                <form class="form">
                                    <div class="form_group">
                                        <div class="form_label">Send to</div>
                                        <div class="currency">
                                            <div class="currency__active">
                                                <div class="currency__active_icon">
                                                    <img src="img/currency__btc.png" class="img-fluid" alt="">
                                                </div>
                                                <div class="currency__active_text"><span>Bitcoin</span></div>
                                                <input class="currency__value" type="hidden" name="currency" value="Bitcoin">
                                            </div>
                                            <div class="currency__list">
                                                <div class="currency__item">
                                                    <div class="currency__item_icon">
                                                        <img src="img/currency__btc.png" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="currency__item_name">Bitcoin</div>
                                                </div>
                                                <div class="currency__item">
                                                    <div class="currency__item_icon">
                                                        <img src="img/currency__ltc.png" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="currency__item_name">Litecoin</div>
                                                </div>
                                                <div class="currency__item">
                                                    <div class="currency__item_icon">
                                                        <img src="img/currency__e.png" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="currency__item_name">Ethereum</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_group">
                                        <div class="form_label">Amount, USD</div>
                                        <input class="form_control" type="text" name="amount" placeholder="" value="4324">
                                    </div>
                                    <div class="form_group">
                                        <div class="form_label">Amount, BTC</div>
                                        <input class="form_control" type="text" name="amount" placeholder="" value="0.46704433" disabled>
                                    </div>
                                    <div class="form_group mb_0">
                                        <div class="form_label">Please send BTC to this address</div>
                                        <input class="form_control" type="text" name="amount" placeholder="" value="3FwMiN3rxkeVXfaoLpLJz1WS5koWRpcW56" disabled>
                                    </div>
                                </form>
                            </div>
                        </div>



                        <div class="payouts hide">
                            <div class="message message_blue mb_25">o confirm and finish withdrawal process - please check your email (also SPAM folder). If you use google authenticator, just ignore this message</div>
                            <form class="form">
                                <div class="row mb_10">
                                    <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                        <div class="form_group">
                                            <div class="form_label">Select balance</div>
                                            <select class="form_control form_select">
                                                <option value="Main balance">Main balance</option>
                                                <option value="Main balance">Main balance</option>
                                                <option value="Main balance">Main balance</option>
                                                <option value="Main balance">Main balance</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                        <div class="form_group">
                                            <div class="form_label">Send to</div>
                                            <div class="currency">
                                                <div class="currency__active">
                                                    <div class="currency__active_icon">
                                                        <img src="img/currency__btc.png" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="currency__active_text"><span>Bitcoin</span></div>
                                                    <input class="currency__value" type="hidden" name="currency" value="Bitcoin">
                                                </div>
                                                <div class="currency__list">
                                                    <div class="currency__item">
                                                        <div class="currency__item_icon">
                                                            <img src="img/currency__btc.png" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="currency__item_name">Bitcoin</div>
                                                    </div>
                                                    <div class="currency__item">
                                                        <div class="currency__item_icon">
                                                            <img src="img/currency__ltc.png" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="currency__item_name">Litecoin</div>
                                                    </div>
                                                    <div class="currency__item">
                                                        <div class="currency__item_icon">
                                                            <img src="img/currency__e.png" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="currency__item_name">Ethereum</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                        <div class="form_group">
                                            <div class="form_label">Amount, USD</div>
                                            <input class="form_control" type="text" name="amount" placeholder="" value="">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                        <div class="form_label"></div>
                                        <div class="message message_red">Please setup your wallet for payouts on page Settings</div>
                                    </div>
                                </div>
                                <ul class="btn_group">
                                    <li>
                                        <button type="submit" class="btn btn_yellow btn_submit">confirm</button>
                                    </li>
                                    <li>
                                        <button type="reset" class="btn">cancel</button>
                                    </li>
                                </ul>
                            </form>
                        </div>

                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>


        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
