<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Money transfer</h1>
                            <div class="heading__time">System time:  <strong>2019-07-16 14:14:48</strong></div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn btn_yellow btn_xs ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col col-xs-12 col-md-6 col-gutter-lr">
                            <div class="white_box mb_30">
                                <h2>Transfer between your wallets</h2>
                                <form class="form">
                                    <div class="form_group">
                                        <div class="form_label">Send from</div>
                                        <select class="form_control form_select" name="n2">
                                            <option value="Main balance">Main balance</option>
                                            <option value="Main balance">Main balance</option>
                                            <option value="Main balance">Main balance</option>
                                            <option value="Main balance">Main balance</option>
                                        </select>
                                    </div>
                                    <div class="form_group">
                                        <div class="form_label">To</div>
                                        <input type="text" class="form_control" name="n1" placeholder="Fund balance">
                                    </div>
                                    <div class="form_group">
                                        <div class="form_label">Amount</div>
                                        <input type="text" class="form_control" name="n1" placeholder="">
                                    </div>
                                    <ul class="btn_group">
                                        <li>
                                            <button type="submit" class="btn btn_yellow btn_submit">exchange</button>
                                        </li>
                                    </ul>
                                </form>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-md-6 col-gutter-lr">
                            <div class="white_box mb_30">
                                <h2>Transfer between accounts</h2>
                                <form class="form">
                                    <div class="form_group">
                                        <div class="form_label">Send from</div>
                                        <select class="form_control form_select">
                                            <option value="Main balance">Main balance</option>
                                            <option value="Main balance">Main balance</option>
                                            <option value="Main balance">Main balance</option>
                                            <option value="Main balance">Main balance</option>
                                        </select>
                                    </div>
                                    <div class="form_group">
                                        <div class="form_label">Enter e-mail or ID of recipient</div>
                                        <input type="text" class="form_control" name="n1" placeholder="" value="vexa2390@gmail.com">
                                    </div>
                                    <div class="form_group">
                                        <div class="form_label">Send to</div>
                                        <input type="text" class="form_control form_control_user" name="name" placeholder="" value="Albert Smith">
                                    </div>
                                    <div class="form_group">
                                        <div class="form_label">Amount</div>
                                        <input type="text" class="form_control" name="n1" placeholder="">
                                    </div>
                                    <ul class="btn_group">
                                        <li>
                                            <button type="submit" class="btn btn_yellow btn_submit">check recipient</button>
                                        </li>
                                    </ul>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>


        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
