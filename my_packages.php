<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>My packages</h1>
                            <div class="heading__time">System time:  <strong>2019-07-16 14:14:48</strong></div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn btn_yellow btn_xs ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <div class="content">

                        <div class="content__header">
                            <div class="content__header_title">
                                <h4>Packages list </h4>
                            </div>
                        </div>

                        <div class="packages tabs">

                            <div class="packages__nav tabs_nav">
                                <a data-target=".tab1" href="#" class="active">
                                    <i>
                                        <img src="img/packages__icon_01.png" alt="">
                                    </i>
                                    <span>basic</span>
                                    <strong>12</strong>
                                </a>
                                <a data-target=".tab2" href="#">
                                    <i>
                                        <img src="img/packages__icon_02.png" alt="">
                                    </i>
                                    <span>premium</span>
                                    <strong>19</strong>
                                </a>
                                <a data-target=".tab3" href="#">
                                    <i>
                                        <img src="img/packages__icon_03.png" alt="">
                                    </i>
                                    <span>exclusive</span>
                                    <strong>4</strong>
                                </a>
                            </div>

                            <div class="packages__item tabs_item tab1 active">
                                <div class="content__table">
                                    <div class="table_responsive">
                                        <table class="table">
                                            <tr>
                                                <th>ID</th>
                                                <th>Amount</th>
                                                <th>Date</th>
                                                <th>Income</th>
                                                <th class="text-nowrap text-right">Total income</th>
                                            </tr>

                                            <tr>
                                                <td class="text-nowrap">VX3927747</td>
                                                <td class="text-nowrap text_semibold">$540.00</td>
                                                <td class="text-nowrap">22 June 2019, at 13:45</td>
                                                <td class="text-nowrap text_semibold">$54.00</td>
                                                <td class="text-nowrap text-right">$54.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927746</td>
                                                <td class="text-nowrap text_semibold">$640.00</td>
                                                <td class="text-nowrap">12 May 2019, at 10:34</td>
                                                <td class="text-nowrap text_semibold">$64.00</td>
                                                <td class="text-nowrap text-right">$64.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927745</td>
                                                <td class="text-nowrap text_semibold">$700.00</td>
                                                <td class="text-nowrap">30 Dec 2018, at 20:39</td>
                                                <td class="text-nowrap text_semibold">$70.00</td>
                                                <td class="text-nowrap text-right">$70.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927744</td>
                                                <td class="text-nowrap text_semibold">$460.00</td>
                                                <td class="text-nowrap">17 Nov 2018, at 10:34</td>
                                                <td class="text-nowrap text_semibold">$46.00</td>
                                                <td class="text-nowrap text-right">$46.00</td>
                                            </tr>

                                            <tr>
                                                <td class="text-nowrap">VX3927747</td>
                                                <td class="text-nowrap text_semibold">$540.00</td>
                                                <td class="text-nowrap">22 June 2019, at 13:45</td>
                                                <td class="text-nowrap text_semibold">$54.00</td>
                                                <td class="text-nowrap text-right">$54.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927746</td>
                                                <td class="text-nowrap text_semibold">$640.00</td>
                                                <td class="text-nowrap">12 May 2019, at 10:34</td>
                                                <td class="text-nowrap text_semibold">$64.00</td>
                                                <td class="text-nowrap text-right">$64.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927745</td>
                                                <td class="text-nowrap text_semibold">$700.00</td>
                                                <td class="text-nowrap">30 Dec 2018, at 20:39</td>
                                                <td class="text-nowrap text_semibold">$70.00</td>
                                                <td class="text-nowrap text-right">$70.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927744</td>
                                                <td class="text-nowrap text_semibold">$460.00</td>
                                                <td class="text-nowrap">17 Nov 2018, at 10:34</td>
                                                <td class="text-nowrap text_semibold">$46.00</td>
                                                <td class="text-nowrap text-right">$46.00</td>
                                            </tr>

                                            <tr>
                                                <td class="text-nowrap">VX3927747</td>
                                                <td class="text-nowrap text_semibold">$540.00</td>
                                                <td class="text-nowrap">22 June 2019, at 13:45</td>
                                                <td class="text-nowrap text_semibold">$54.00</td>
                                                <td class="text-nowrap text-right">$54.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927746</td>
                                                <td class="text-nowrap text_semibold">$640.00</td>
                                                <td class="text-nowrap">12 May 2019, at 10:34</td>
                                                <td class="text-nowrap text_semibold">$64.00</td>
                                                <td class="text-nowrap text-right">$64.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927745</td>
                                                <td class="text-nowrap text_semibold">$700.00</td>
                                                <td class="text-nowrap">30 Dec 2018, at 20:39</td>
                                                <td class="text-nowrap text_semibold">$70.00</td>
                                                <td class="text-nowrap text-right">$70.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927744</td>
                                                <td class="text-nowrap text_semibold">$460.00</td>
                                                <td class="text-nowrap">17 Nov 2018, at 10:34</td>
                                                <td class="text-nowrap text_semibold">$46.00</td>
                                                <td class="text-nowrap text-right">$46.00</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="packages__item tabs_item tab2">
                                <div class="content__table">
                                    <div class="table_responsive">
                                        <table class="table">
                                            <tr>
                                                <th>ID</th>
                                                <th>Amount</th>
                                                <th>Date</th>
                                                <th>Income</th>
                                                <th class="text-nowrap text-right">Total income</th>
                                            </tr>

                                            <tr>
                                                <td class="text-nowrap">VX3927747</td>
                                                <td class="text-nowrap text_semibold">$540.00</td>
                                                <td class="text-nowrap">22 June 2019, at 13:45</td>
                                                <td class="text-nowrap text_semibold">$54.00</td>
                                                <td class="text-nowrap text-right">$54.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927746</td>
                                                <td class="text-nowrap text_semibold">$640.00</td>
                                                <td class="text-nowrap">12 May 2019, at 10:34</td>
                                                <td class="text-nowrap text_semibold">$64.00</td>
                                                <td class="text-nowrap text-right">$64.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927745</td>
                                                <td class="text-nowrap text_semibold">$700.00</td>
                                                <td class="text-nowrap">30 Dec 2018, at 20:39</td>
                                                <td class="text-nowrap text_semibold">$70.00</td>
                                                <td class="text-nowrap text-right">$70.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927744</td>
                                                <td class="text-nowrap text_semibold">$460.00</td>
                                                <td class="text-nowrap">17 Nov 2018, at 10:34</td>
                                                <td class="text-nowrap text_semibold">$46.00</td>
                                                <td class="text-nowrap text-right">$46.00</td>
                                            </tr>

                                            <tr>
                                                <td class="text-nowrap">VX3927747</td>
                                                <td class="text-nowrap text_semibold">$540.00</td>
                                                <td class="text-nowrap">22 June 2019, at 13:45</td>
                                                <td class="text-nowrap text_semibold">$54.00</td>
                                                <td class="text-nowrap text-right">$54.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927746</td>
                                                <td class="text-nowrap text_semibold">$640.00</td>
                                                <td class="text-nowrap">12 May 2019, at 10:34</td>
                                                <td class="text-nowrap text_semibold">$64.00</td>
                                                <td class="text-nowrap text-right">$64.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927745</td>
                                                <td class="text-nowrap text_semibold">$700.00</td>
                                                <td class="text-nowrap">30 Dec 2018, at 20:39</td>
                                                <td class="text-nowrap text_semibold">$70.00</td>
                                                <td class="text-nowrap text-right">$70.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927744</td>
                                                <td class="text-nowrap text_semibold">$460.00</td>
                                                <td class="text-nowrap">17 Nov 2018, at 10:34</td>
                                                <td class="text-nowrap text_semibold">$46.00</td>
                                                <td class="text-nowrap text-right">$46.00</td>
                                            </tr>

                                            <tr>
                                                <td class="text-nowrap">VX3927747</td>
                                                <td class="text-nowrap text_semibold">$540.00</td>
                                                <td class="text-nowrap">22 June 2019, at 13:45</td>
                                                <td class="text-nowrap text_semibold">$54.00</td>
                                                <td class="text-nowrap text-right">$54.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927746</td>
                                                <td class="text-nowrap text_semibold">$640.00</td>
                                                <td class="text-nowrap">12 May 2019, at 10:34</td>
                                                <td class="text-nowrap text_semibold">$64.00</td>
                                                <td class="text-nowrap text-right">$64.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927745</td>
                                                <td class="text-nowrap text_semibold">$700.00</td>
                                                <td class="text-nowrap">30 Dec 2018, at 20:39</td>
                                                <td class="text-nowrap text_semibold">$70.00</td>
                                                <td class="text-nowrap text-right">$70.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927744</td>
                                                <td class="text-nowrap text_semibold">$460.00</td>
                                                <td class="text-nowrap">17 Nov 2018, at 10:34</td>
                                                <td class="text-nowrap text_semibold">$46.00</td>
                                                <td class="text-nowrap text-right">$46.00</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="packages__item tabs_item tab3">
                                <div class="content__table">
                                    <div class="table_responsive">
                                        <table class="table">
                                            <tr>
                                                <th>ID</th>
                                                <th>Amount</th>
                                                <th>Date</th>
                                                <th>Income</th>
                                                <th class="text-nowrap text-right">Total income</th>
                                            </tr>

                                            <tr>
                                                <td class="text-nowrap">VX3927747</td>
                                                <td class="text-nowrap text_semibold">$540.00</td>
                                                <td class="text-nowrap">22 June 2019, at 13:45</td>
                                                <td class="text-nowrap text_semibold">$54.00</td>
                                                <td class="text-nowrap text-right">$54.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927746</td>
                                                <td class="text-nowrap text_semibold">$640.00</td>
                                                <td class="text-nowrap">12 May 2019, at 10:34</td>
                                                <td class="text-nowrap text_semibold">$64.00</td>
                                                <td class="text-nowrap text-right">$64.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927745</td>
                                                <td class="text-nowrap text_semibold">$700.00</td>
                                                <td class="text-nowrap">30 Dec 2018, at 20:39</td>
                                                <td class="text-nowrap text_semibold">$70.00</td>
                                                <td class="text-nowrap text-right">$70.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927744</td>
                                                <td class="text-nowrap text_semibold">$460.00</td>
                                                <td class="text-nowrap">17 Nov 2018, at 10:34</td>
                                                <td class="text-nowrap text_semibold">$46.00</td>
                                                <td class="text-nowrap text-right">$46.00</td>
                                            </tr>

                                            <tr>
                                                <td class="text-nowrap">VX3927747</td>
                                                <td class="text-nowrap text_semibold">$540.00</td>
                                                <td class="text-nowrap">22 June 2019, at 13:45</td>
                                                <td class="text-nowrap text_semibold">$54.00</td>
                                                <td class="text-nowrap text-right">$54.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927746</td>
                                                <td class="text-nowrap text_semibold">$640.00</td>
                                                <td class="text-nowrap">12 May 2019, at 10:34</td>
                                                <td class="text-nowrap text_semibold">$64.00</td>
                                                <td class="text-nowrap text-right">$64.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927745</td>
                                                <td class="text-nowrap text_semibold">$700.00</td>
                                                <td class="text-nowrap">30 Dec 2018, at 20:39</td>
                                                <td class="text-nowrap text_semibold">$70.00</td>
                                                <td class="text-nowrap text-right">$70.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927744</td>
                                                <td class="text-nowrap text_semibold">$460.00</td>
                                                <td class="text-nowrap">17 Nov 2018, at 10:34</td>
                                                <td class="text-nowrap text_semibold">$46.00</td>
                                                <td class="text-nowrap text-right">$46.00</td>
                                            </tr>

                                            <tr>
                                                <td class="text-nowrap">VX3927747</td>
                                                <td class="text-nowrap text_semibold">$540.00</td>
                                                <td class="text-nowrap">22 June 2019, at 13:45</td>
                                                <td class="text-nowrap text_semibold">$54.00</td>
                                                <td class="text-nowrap text-right">$54.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927746</td>
                                                <td class="text-nowrap text_semibold">$640.00</td>
                                                <td class="text-nowrap">12 May 2019, at 10:34</td>
                                                <td class="text-nowrap text_semibold">$64.00</td>
                                                <td class="text-nowrap text-right">$64.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927745</td>
                                                <td class="text-nowrap text_semibold">$700.00</td>
                                                <td class="text-nowrap">30 Dec 2018, at 20:39</td>
                                                <td class="text-nowrap text_semibold">$70.00</td>
                                                <td class="text-nowrap text-right">$70.00</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">VX3927744</td>
                                                <td class="text-nowrap text_semibold">$460.00</td>
                                                <td class="text-nowrap">17 Nov 2018, at 10:34</td>
                                                <td class="text-nowrap text_semibold">$46.00</td>
                                                <td class="text-nowrap text-right">$46.00</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>


        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
